package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Reports_SalesOrdersPage extends BasePage {

    @FindBy(xpath = "//div[1]/div[1]/mat-form-field[1]/div[1]/div[1]/div[3]//input[@placeholder='Choose a date']")
    public WebElement startDateBox;

    @FindBy(xpath = "//div[2]/div[1]/mat-form-field[1]/div[1]/div[1]/div[3]/input[@placeholder='Choose a date']")
    public WebElement endDateBox;

    @FindBy(xpath = "//*[@placeholder='Search dealer']")
    public WebElement dealerBox;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    public WebElement filterButton;

    @FindBy(xpath = "//button[@class='btn btn-primary ng-star-inserted']")
    public WebElement clickOnSendEmail;

    @FindBy(xpath = "//button[@class='btn btn-primary'][contains(text(),'Send')]")
    public  WebElement clickOnSend;

    @FindBy(xpath = "//tr[1]/td[9][@class='text-danger']")
    public WebElement verifyTotalBalanceAmount;



    public void setStartDateBox(String str) {
        BrowserUtils.waitForStaleElement(startDateBox);
        for (int i = 0; i <10; i++) {
            startDateBox.sendKeys(Keys.BACK_SPACE);
        }
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        startDateBox.sendKeys(str);
    }

    public void setEndDateBox(String str) {
        BrowserUtils.waitForStaleElement(endDateBox);
        for (int i = 0; i <15; i++) {
            endDateBox.sendKeys(Keys.BACK_SPACE);
        }
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        endDateBox.sendKeys(str);
    }

    public void setDealerBox(String str) {
        BrowserUtils.waitForStaleElement(dealerBox);
        dealerBox.clear();
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        dealerBox.sendKeys(str);

    }

    public void clickFilterButton() {
        BrowserUtils.wait(1);
        filterButton.click();

    }

    public void VerifyTheFilteredDealer(String verify) {
        BrowserUtils.wait(1);
        WebElement verifyDealer = Driver.get().findElement(By.xpath("//a[contains(text(),'" + verify + "')]"));
        BrowserUtils.wait(1);
        Assert.assertEquals(verify, verifyDealer.getText());
        highLightElement(dealerBox);
        highLightElement(verifyDealer);
        System.out.println("Verified Dealer is: " + verifyDealer.getText());
    }

    public void clickOnSendEmailButton() {
        BrowserUtils.wait(1);
        clickOnSendEmail.click();
    }

    public void clickOnSendButton() {
        BrowserUtils.wait(1);
        clickOnSend.click();
        BrowserUtils.wait(2);
    }



    public void verifyTotalBalanceAmountSum() {
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement verifyBalance = null;
        String getAmount;
        String getAmountLast;
        String getTotalBalanceLast;
        Double totalSumDouble = 0.0d;
        Double totalSumDoubleAll = 0.0d;

        List<WebElement> balance = Driver.get().findElements(By.xpath("//td[@class='text-danger']"));
        System.out.println(balance.size());

        for (int i = 2; i <= balance.size() ; i++) {

            try {
                verifyBalance = Driver.get().findElement(By.xpath("//tr[" + i + "]/td[9][@class='text-danger']"));
            }catch (Exception ignored){
                ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].scrollIntoView(true);", verifyBalance);
                try {
                verifyBalance = Driver.get().findElement(By.xpath("//tr[" + i + "]/td[9][@class='text-danger']"));
                }catch (Exception ignore){
                    break;
                }
            }



                getAmount = verifyBalance.getText().replace("$", "");

            if (getAmount.contains(",")) {
                getAmountLast = getAmount.replace(",", "");
            }else{
                getAmountLast = getAmount;
                }
            totalSumDouble = Double.parseDouble(getAmountLast.trim());

                totalSumDoubleAll += totalSumDouble;
            System.out.println("Total Balance is : " + totalSumDoubleAll);
        }

        String getTotalBalance = verifyTotalBalanceAmount.getText().replace("$", "");

        if (getTotalBalance.contains(",")) {
            getTotalBalanceLast = getTotalBalance.replace(",", "");
        }else{
            getTotalBalanceLast = getTotalBalance;
        }
        Double totalAmountDouble = Double.parseDouble(getTotalBalanceLast.trim());
        System.out.println("Last Total Balance is : " + totalSumDoubleAll);

        Assert.assertEquals(totalAmountDouble,totalSumDoubleAll);
        highLightElement(verifyTotalBalanceAmount);
//        if (totalAmountDouble==totalSumDoubleAll) {
//            BrowserUtils.wait(1);
//            highLightElement(verifyTotalBalanceAmount);
//            BrowserUtils.wait(2);
//            System.out.println("Verified Total Balance is: " + verifyTotalBalanceAmount.getText());
//        }else{
//            //System.out.println("Total Balance was not verify.");
//        }



    }

}
