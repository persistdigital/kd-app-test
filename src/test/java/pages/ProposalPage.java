package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Set;

public class ProposalPage extends BasePage {

    public ProposalPage() {

        PageFactory.initElements(Driver.get(), this);
    }

@FindBy(xpath = "//span[contains(text(),'Filters')]") // filtrelemeyi baslatmak icin
public WebElement filtersToggle;

    @FindBy(xpath = "//button[contains(text(),'Filter')]")  // filtrelemeyi onaylamak icin
    public WebElement filterButton;

    @FindBy(xpath = "//*[contains(@class,'mat-autocomplete-')]")
    public WebElement filterDealerName;

    @FindBy(xpath = "(//*[contains(@class,'mat-select-value')])[1]")
    public WebElement filterStatus;

    @FindBy(xpath = "//span[contains(text(),'Draft')]")
    public WebElement filterStatusDraft;

    @FindBy(xpath = "(//td)[1]")
    public WebElement proposalID;

    @FindBy(xpath = "//*[contains(text(),'more')]")
    public WebElement threeDot;

    @FindBy(xpath = "//*[contains(text(),'Remove')]")
     public WebElement removeProposalButton;

     @FindBy(xpath = "//span[contains(text(),'Delete')]")
    public WebElement deleteButton;

    @FindBy(css = "[class='btn btn-primary']")
    public WebElement createProposalButton;

    @FindBy(css = "[role='combobox']")
    public WebElement selectDealerBox;

    @FindBy(css = "[class = 'mat-option-text']")
    public WebElement selectDealerLocation;

    @FindBy(css = "[class='mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored ng-untouched ng-pristine ng-invalid']")
    public WebElement projectDescription;

    @FindBy(xpath = "(//*[starts-with(@class,'mat-select-placeholder ng')])[1]")
    public WebElement salesRepresentative;

    @FindBy(xpath = "(//*[starts-with(@class,'mat-select-placeholder ng')])[2]")
    public WebElement costumerServiceRepresentative;

    @FindBy(xpath = "//*[contains(text(),'Next')]")
    public WebElement nextButton;

    @FindBy(xpath = "//*[starts-with(@class,'mat-select ng')]")
    public WebElement selectCollection;

    @FindBy(xpath = "//*[starts-with(@class,'mat-input')]")
    public WebElement versionName;

    @FindBy(xpath = "//*[@placeholder='Enter part code or description']")
    public WebElement enterPartCode;

    @FindBy(xpath = "//*[contains(text(),'Save & Close')]")
    public WebElement saveAndCloseButton;

    @FindBy(xpath = "//*[@class='mat-error ng-star-inserted']")
    public WebElement dealerErrorMessage;

    @FindBy(xpath = "//*[@class='mat-error ng-star-inserted'][contains(text(),'Please select location')]")
    public WebElement locationErrorMessage;

    @FindBy(xpath = "//*[@class='mat-error ng-star-inserted'][contains(text(),'Description is required')]")
    public WebElement descriptionErrorMessage;

    @FindBy(xpath = "//*[@class='mat-error ng-star-inserted'][contains(text(),'Sales representative is required')]")
    public WebElement salesRepresentativeErrorMessage;

    @FindBy(xpath = "//*[@class='mat-error ng-star-inserted'][contains(text(),'Collection is required')]")
    public WebElement collectionErrorMessage;

    @FindBy(xpath = "//*[@class='mat-error ng-star-inserted'][contains(text(),'Version name is required')]")
    public WebElement versionErrorMessage;

    public void createProposal(String companyName, String collectionName, String version, String part) {

//first page
        createProposalButton.click();
        BrowserUtils.wait(2);

        selectDealerBox.click();
        BrowserUtils.wait(2);
        WebElement selectCompany = Driver.get().findElement(By.xpath("//span[text()='" + companyName + "']"));
        selectCompany.click();
        BrowserUtils.wait(3);


        projectDescription.sendKeys(version + " / " + collectionName);
        BrowserUtils.wait(3);

        JavascriptExecutor js = (JavascriptExecutor) Driver.get();
        js.executeScript("window.scrollBy(0,1000)");
        BrowserUtils.wait(3);


        salesRepresentative.click();
        BrowserUtils.wait(2);
        List<WebElement> salesRepresent = Driver.get().findElements(By.xpath("(//*[contains(@class,'mat-option ')])[3]"));
        for (WebElement option : salesRepresent) {
            if (option.getText().contains("galale")) {
                option.click();
            }
        }

        BrowserUtils.wait(2);

        nextButton.submit();

        //second page
        BrowserUtils.wait(1);
        js.executeScript("window.scrollBy(0,-500)");
        BrowserUtils.wait(2);
        selectCollection.click();
        BrowserUtils.wait(2);

        List<WebElement> options = Driver.get().findElements(By.xpath("//span[@class='mat-option-text']"));
        for (WebElement option : options) {
            if (option.getText().contains(collectionName)) {
                option.click();
            }
        }
        BrowserUtils.wait(2);
        versionName.sendKeys(version);
        BrowserUtils.wait(1);
        nextButton.submit();
        BrowserUtils.wait(1);

        // third page
        js.executeScript("window.scrollBy(0,500)");

        enterPartCode.click();

        List<WebElement> dropdown_list = Driver.get().findElements(By.cssSelector("[id*='mat-option']"));

        // Printing the amount of WebElements inside the list
        //  System.out.println("The Options in the Part's are: " + dropdown_list.size());


        for (int i = 0; i < dropdown_list.size(); i++) {
            // Printing All the options from the dropdown
            //  System.out.println(dropdown_list.get(i).getText());

            if (dropdown_list.get(i).getText().contains(part)) {

                dropdown_list.get(i).click();
                break;
            }
        }

        js.executeScript("window.scrollBy(0,1000)");
        BrowserUtils.wait(2);

        saveAndCloseButton.click();

        System.out.println("proposal was created from "+companyName);
    }

    public void createProposal_Step1(String companyName) {

//first page
        selectDealerBox.click();
        BrowserUtils.wait(2);
        WebElement selectCompany = Driver.get().findElement(By.xpath("//span[text()='" + companyName + "']"));
        selectCompany.click();
        BrowserUtils.wait(3);


        projectDescription.sendKeys("description");
        BrowserUtils.wait(2);

        JavascriptExecutor js = (JavascriptExecutor) Driver.get();
        js.executeScript("window.scrollBy(0,1000)");
        BrowserUtils.wait(2);


        salesRepresentative.submit();
        BrowserUtils.wait(2);
        costumerServiceRepresentative.submit();

        BrowserUtils.wait(2);

        nextButton.click();

    }

    public String getWarningMessage(String message){
        WebElement warningMessage = Driver.get().findElement(By.xpath("//*[contains(text(),'"+message+"')]"));
        String actualMessage = warningMessage.getText();
        return actualMessage;
    }


    public void deleteProposal(String proposalCompanyName){
        BrowserUtils.wait(2);
        filtersToggle.click();
        BrowserUtils.wait(1);
        filterDealerName.sendKeys(proposalCompanyName);
        BrowserUtils.wait(1);
        filterButton.click();
BrowserUtils.wait(2);
        WebElement number = Driver.get().findElement(By.xpath("//*[@class='mat-paginator-range-label']"));
        String totalProposalNumber = number.getText();

        totalProposalNumber=totalProposalNumber.substring(totalProposalNumber.lastIndexOf(" ")+1);

        System.out.println("total proposal which you wanna delete number is: " + totalProposalNumber);
        BrowserUtils.wait(2);
        int totalProposalNum = Integer.parseInt(totalProposalNumber);

        int count = 0;
        while (count < totalProposalNum) {

            List <WebElement> proposalsList = Driver.get().findElements(By.xpath("//*[@class='mat-cell cdk-column-name mat-column-name ng-star-inserted']"));

            if (proposalsList.get(0).getText().contains(proposalCompanyName)) {
                String proposalId = proposalID.getText();

                JavascriptExecutor js = (JavascriptExecutor) Driver.get();
                js.executeScript("scroll(200, 500);");
                BrowserUtils.wait(2);
                threeDot.click();

                BrowserUtils.wait(3);
                removeProposalButton.click();
                BrowserUtils.wait(3);

                String oldWindow = Driver.get().getWindowHandle();
                BrowserUtils.wait(2);
                Set<String> windowHandles = Driver.get().getWindowHandles();

                for (String windowHandle : windowHandles) {
                    if (!windowHandle.equals(oldWindow)) {
                        Driver.get().switchTo().window(windowHandle);
                    }
                }
                System.out.println("Proposal ID "+ proposalId+ " was deleted");
                deleteButton.click();
                BrowserUtils.wait(10);

                count++;


            } else{
                System.out.println("There are not much more proposals which matches your search");
                break;
            }
        }
    }

    public void deleteProposalByProposalID(String proposalId){

        List<WebElement> proposalIDList = Driver.get().findElements(By.xpath("//td"));
int numberOfElement = proposalIDList.size();

for(int i=0; i< numberOfElement; i++) {

    if (proposalIDList.get(i).getText().equals(proposalId)) {
        System.out.println(proposalIDList.get(i).getText());

        BrowserUtils.wait(3);
        threeDot.click();

        BrowserUtils.wait(3);
        removeProposalButton.click();
        BrowserUtils.wait(3);

        String oldWindow = Driver.get().getWindowHandle();
        BrowserUtils.wait(2);
        Set<String> windowHandles = Driver.get().getWindowHandles();

        for (String windowHandle : windowHandles) {
            if (!windowHandle.equals(oldWindow)) {
                Driver.get().switchTo().window(windowHandle);
            }
        }
        System.out.println("Proposal ID " + proposalId + " was deleted");
        deleteButton.click();
        BrowserUtils.wait(10);
        break;
    }
}
    }
}