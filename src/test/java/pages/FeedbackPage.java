package pages;

import Utulities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FeedbackPage extends BasePage{
    @FindBy(xpath = "//*[@id='mat-input-3']")
    WebElement messageInputBox;



public void selectDealer(String dealer){
    String dealerXpath="//*[contains(text(),'"+dealer+"')]";
    Driver.get().findElement(By.xpath(dealerXpath)).click();

}
public void sendMessage(String message){
    messageInputBox.sendKeys(message);
}
public String actualMessage(String message){
    String messageXpath="//*[contains(text(),'"+message+"')]";
    String sentMessage =Driver.get().findElement(By.xpath(messageXpath)).getText();
    return sentMessage;
}



}
