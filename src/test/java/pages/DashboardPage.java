package pages;

import Utulities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class DashboardPage extends BasePage {
    @FindBy(css = "[href=\"/app/vendor/dashboard/quick-access\"]")
    public WebElement quickAccess;
    @FindBy(css = "[class=\"mat-button-wrapper\"]")
    public WebElement history;
    @FindBy(css = "[href=\"/app/vendor/dashboard/catch-up\"]")
    public WebElement catchUp;
    @FindBy(css = "[class=\"orders cp\"] [class=\"content-container\"]:nth-of-type(1) p")
    public WebElement activeOrders;
    @FindBy(css = "[class=\"dealers cp\"] [class=\"content-container\"]:nth-of-type(1) p")
    public WebElement dealers;
    @FindBy(css = "[class=\"active cp\"] [class=\"content-container\"]:nth-of-type(1) p")
    public WebElement activeRMAs;
    @FindBy(css = "[class=\"wrapper\"]")
    public List<WebElement> list1;
    @FindBy(css = "[class=\"navigation\"] ul")
    public List<WebElement> list2;


    public DashboardPage(){
        PageFactory.initElements(Driver.get(),this);
    }



    public void verify(String verify) {
        String moduleLocator = "//*[contains(text(),'" + verify + "')]";
    }


    }
