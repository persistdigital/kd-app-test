package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Settings_TaxesPage extends BasePage {

    @FindBy(xpath = "//h3[contains(.,'')]")
    public WebElement pageHeaderTaxes;

    @FindBy(xpath = "//*[@placeholder='Enter tax label']")
    public WebElement taxLabelInput;

    @FindBy(xpath = "//*[@placeholder='Enter tax value']")
    public WebElement taxValueInput;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    public WebElement addTaxButton;

    @FindBy(xpath = "//div[5]/mat-form-field[1]/div[1]/div[1]/div[1]/input[@type='text']")
    public WebElement verifyCreatedLabel;

    @FindBy(xpath = "//div[5]/mat-form-field[1]/div[1]/div[1]/div[1]/input[@type='text']")
    public WebElement editTaxLabel;

    @FindBy(xpath = "//div[5]/mat-form-field[2]/div[1]/div[1]/div[1]//input[@type='number']")
    public WebElement editTaxValue;

    @FindBy(xpath = "//div[5]//span[1]//i[1]")
    public WebElement clickOnX;

    @FindBy(xpath = "//span[contains(text(),'Taxes')]")
    public WebElement clickOnTaxesSubmodule;

    public void setTaxLabelInput(String str){
        highLightElement(pageHeaderTaxes);
        taxLabelInput.sendKeys(str);
    }

    public void setTaxValueInput(String str){
        BrowserUtils.wait(1);
        taxValueInput.sendKeys(str);
    }

    public void clickAddTaxButton(){
        BrowserUtils.wait(1);
        addTaxButton.click();
        BrowserUtils.wait(2);
    }

    public void highlightVerifyCreatedLabel(){
        highLightElement(verifyCreatedLabel);
    }

    public void setEditTaxLabel(String str){
        BrowserUtils.wait(1);
        editTaxLabel.clear();
        editTaxLabel.sendKeys(str);
    }

    public void setEditTaxValue(String str){
        BrowserUtils.wait(1);
        editTaxValue.clear();
        editTaxValue.sendKeys(str);
        BrowserUtils.wait(1);
    }

    public void highlightVerifyEditedLabelAndValue(){
        highLightElement(editTaxLabel);
        highLightElement(editTaxValue);
    }

    public void clickOnXToDelete(){
        clickOnX.click();
        BrowserUtils.wait(2);
    }


    public void clickOnTaxesSubmoduleOfSettings(){
        ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].scrollIntoView(true);", clickOnTaxesSubmodule);
        BrowserUtils.wait(1);
        clickOnTaxesSubmodule.click();
        BrowserUtils.wait(1);
    }








}
