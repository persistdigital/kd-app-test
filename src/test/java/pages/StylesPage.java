package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Set;

public class StylesPage extends BasePage {

    @FindBy(css = "[title='Create new Collection']")
    public WebElement createNewCollectionIcon;

    @FindBy(xpath = "/html[1]/body[1]/m-pages[1]/div[1]/div[1]/div[1]/ng-component[1]/m-styles[1]/div[1]/div[5]/button[1]")
    public WebElement createNewSeriesIcon;

    @FindBy(css = "[class='btn btn-primary']")
    public WebElement createButton;

    @FindBy(css="[class='flaticon-cancel']")
    public WebElement cancelSign;

    @FindBy(xpath = "[(contains(text(),'Name is required')]")
    public WebElement errorMessage;

    @FindBy(xpath= "/html[1]/body[1]/div[6]/div[2]/div[1]/mat-dialog-container[1]/m-create-series-dialog[1]/div[1]/div[2]/m-series-form-data[1]/form[1]/div[1]/div[1]/div[1]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]")
    public WebElement nameBox;

    @FindBy(xpath = "(//input[@type='text'])[1]")
    public WebElement collectionName;

    @FindBy(xpath = "(//input[@type='text'])[2]")
    public WebElement collectionShortName;

    @FindBy(xpath = "(//input[@type='text'])[3]")
    public WebElement priceGroup;

    @FindBy(xpath = "(//*[@class='mat-error ng-star-inserted'])[1]")
    public WebElement nameRequiredErrorMessage;

    @FindBy(xpath = "(//*[@class='mat-error ng-star-inserted'])[2]")
    public WebElement shortNameRequiredErrorMessage;

    @FindBy(xpath = "(//*[@class='mat-error ng-star-inserted'])[3]")
    public WebElement priceGroupRequiredErrorMessage;




    public void clickCreateNewSeriesIcon() {
        BrowserUtils.wait(2);
        JavascriptExecutor js = (JavascriptExecutor) Driver.get();
        js.executeScript("window.scrollBy(0,2000)");
        BrowserUtils.wait(2);

        createNewSeriesIcon.click();
        BrowserUtils.wait(1);}

    public void createNewSeries(String name){

        nameBox.sendKeys(name);
    }
    public void clickCreateButton(){
        BrowserUtils.wait(2);
        Assert.assertTrue(createButton.isDisplayed());
    createButton.click();
    }
    public void clickCancelSign(){
    cancelSign.click();
    }

    public void clickCreateCollectionButton(String string){
        BrowserUtils.wait(1);
        String oldWindow = Driver.get().getWindowHandle();
        BrowserUtils.wait(2);
        Set<String> windowHandles = Driver.get().getWindowHandles();

        for (String windowHandle : windowHandles) {
            if (!windowHandle.equals(oldWindow)) {
                Driver.get().switchTo().window(windowHandle);
            }
        }

        WebElement button = Driver.get().findElement(By.xpath("//h3[contains(text(),'"+string+"')]/../following-sibling::mat-card/div[2]/button"));
        button.click();
    }

    public void giveCollectionName(String string){
        BrowserUtils.wait(2);
        collectionName.sendKeys(string);
    }
    public void giveCollectionShortName(String shortName){
        BrowserUtils.wait(2);
        collectionShortName.sendKeys(shortName);
    }
    public void givePriceGroup(String string){
        BrowserUtils.wait(4);
        priceGroup.sendKeys(string);
    }



}
