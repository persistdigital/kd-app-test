package pages;

import Utulities.BrowserUtils;
import Utulities.ConfigurationReader;
import Utulities.Driver;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.HashMap;
import java.util.Map;

public class LoginPage extends BasePage {
    @FindBy(id = "mat-input-0") //this line will initialize web element
    public WebElement emailInput;

    @FindBy(id = "mat-input-1")//without findby, web element will be null
    public WebElement passwordInput;

    @FindBy(className = "mat-button-wrapper")
    public WebElement loginButton;

    @FindBy(xpath = "//span[contains(text(),'Invalid email')]")
    public WebElement passwordWarningMessage;

    @FindBy(xpath = "//span[contains(text(),'Email is not valid')]")
    public WebElement emailWarningMessage;

    @FindBy(xpath = "//a[contains(text(),'About')]")
    public WebElement aboutButton;

    @FindBy(xpath = "//a[contains(text(),'Contact')]")
    public WebElement contactButton;

    @FindBy(xpath = "//a[contains(text(),'Forgot Password?')]")
    public WebElement forgotPassword;

    // it use when forgot password to achieve new one. this locator at the other page not login
    @FindBy(xpath = "//mat-label[contains(text(),'Enter your email address')]")
    public WebElement enterEmailAddress;

    public LoginPage() {
        PageFactory.initElements(Driver.get(), this);
    }

    public void login(String email, String password) {
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password, Keys.ENTER);
    }

    public void login() {
        login(ConfigurationReader.getProperty("email"), ConfigurationReader.getProperty("password"));
    }

    // login with role
    public void login(String role) {
        String email = "";
        String password = "";

        switch (role) {
            case "retailer":
                email = ConfigurationReader.getProperty("retailer_email");
                password = ConfigurationReader.getProperty("retailer_password");
                break;
            case "manufacturer":
                email = ConfigurationReader.getProperty("manufacturer_email");
                password = ConfigurationReader.getProperty("manufacturer_password");
                break;

            default:
                throw new RuntimeException("Invalid role!");
        }
        login(email, password);

    }

    public String enterYourEmail() {
        BrowserUtils.wait(1);
        String label = enterEmailAddress.getText();
        return label;
    }

}