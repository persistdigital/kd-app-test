package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AnnouncementPage {
   public AnnouncementPage(){
       PageFactory.initElements(Driver.get(),this);
   }

   @FindBy(xpath = "(//*[@type='button'])[1]")
    public WebElement newAnnouncementButton;

   @FindBy(xpath = "//*[@id='mat-input-3']")
   public WebElement titleBox;


   @FindBy(xpath = "//*[@id='mat-input-4']")
   public WebElement contentBox;


    @FindBy(xpath = "//span[contains(text(),'Delete announcement')]")
    public WebElement deleteAnnouncement;

    @FindBy(xpath = "//span[contains(text(),'Edit announcement')]")
    public WebElement editAnnouncement;


    @FindBy(xpath = " //button//span[contains(text(),'Delete')]")
    public WebElement deleteButton;

    @FindBy(xpath = "//*[@type='text']")
    public WebElement searchButton;

    public void createNewAnnouncement (){
       newAnnouncementButton.click();
       BrowserUtils.wait(2);
   }

   public void setTitleBox(String str){
       BrowserUtils.wait(2);
       titleBox.sendKeys(str);

   }


   public void setContentBox(String str){
       BrowserUtils.wait(2);
       contentBox.sendKeys(str);

   }
    public void setThreedot(){
        BrowserUtils.wait(10);
        WebElement hover=Driver.get().findElement(By.xpath("//*[@class='actions-hover']"));

        WebElement Ucnokta= Driver.get().findElement(By.xpath("//body//div//div//div//div//div[1]//div[1]//span[1]//button[1]//span[1]//mat-icon[1]"));

        JavascriptExecutor js = (JavascriptExecutor)Driver.get();
        BrowserUtils.wait(5);
        js.executeScript("arguments[0].click();",Ucnokta);

    }


    public void setDeleteAnnouncement(){



      JavascriptExecutor js = (JavascriptExecutor)Driver.get();
        BrowserUtils.wait(5);
        js.executeScript("arguments[0].click();",deleteAnnouncement);
        BrowserUtils.wait(4);


    }

    public void setEditAnnouncement() {
        BrowserUtils.wait(2);
     editAnnouncement.click();
    }

    public void setDeleteButton() {

        JavascriptExecutor js = (JavascriptExecutor)Driver.get();
        BrowserUtils.wait(5);
        js.executeScript("arguments[0].click();",deleteButton);
        BrowserUtils.wait(4);


    }

    public void setSearchButton(String str) {
        BrowserUtils.wait(2);
        searchButton.sendKeys(str);
    }

    public void createButton(){
        WebElement createButtn =Driver.get().findElement(By.xpath("//*[@type='submit']"));
        createButtn.click();
    }

    public void addDeleteAnnouncement (){

      createNewAnnouncement();
        setTitleBox("Announcement1");
        setContentBox("Happy New Year");
        createButton();
        setThreedot();
        setDeleteAnnouncement();
        setDeleteButton();

    }


}
