package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Set;

    public class Retailer_customersPage extends BasePage {

        public Retailer_customersPage() {
            PageFactory.initElements(Driver.get(), this);
        }

        @FindBy(xpath = "//*[contains(text(),'New customer')]")
        public WebElement newCustomerButton;

        @FindBy(xpath = "(//*[starts-with(@class,'mat-input')])[1]")
        public WebElement customerName;

        @FindBy(xpath = "//*[contains(text(),'Submit')]")
        public WebElement submitButton;

        //*[contains(@placeholder,'Filter')]
        //*[@placeholder='Filter by name or email']  ya da  //*[starts-with(@id,'mat')])[4]
        @FindBy(css = "[placeholder='Filter by name or email']")
        public WebElement filter;

        @FindBy(xpath = "//*[@class='mat-icon notranslate material-icons mat-icon-no-color']")
        public WebElement threeDot;


        @FindBy(xpath = "//*[contains(text(),'Remove customer')]")
        public WebElement removeCustomerButton;


        @FindBy(xpath = "(//*[contains(text(),'Delete')])[2]")
        public WebElement deleteButton;

        public void clickNewCustomerButton() {
            newCustomerButton.click();
            BrowserUtils.wait(1);
        }

        public void setCustomerName(String name) {
            customerName.sendKeys(name);
            BrowserUtils.wait(1);
        }

        public void clickSubmitButton() {
            submitButton.submit();
        }

        // bir ve birden fazla customerı silmek icin
        public void deleteCustomers(String customerName) {
            BrowserUtils.wait(4);

            filter.sendKeys(customerName);
            BrowserUtils.wait(2);

            WebElement number = Driver.get().findElement(By.xpath("//*[@class='mat-paginator-range-label']"));
            String totalPartNumber = number.getText();

            System.out.println(totalPartNumber);
            totalPartNumber = totalPartNumber.substring(totalPartNumber.lastIndexOf(" ") + 1);

            System.out.println("total customers which you wanna delete number is: " + totalPartNumber);
            BrowserUtils.wait(2);
            int totalDealerNum = Integer.parseInt(totalPartNumber);

            int count = 0;
            while (count < totalDealerNum) {

                List<WebElement> dealersList = Driver.get().findElements(By.xpath("//*[@class='mat-cell cdk-column-name mat-column-name ng-star-inserted']"));

                if (dealersList.get(0).getText().contains(customerName)) {

                    BrowserUtils.wait(5);


                    Actions actions = new Actions(Driver.get());
                    actions.moveToElement(threeDot).click().build().perform();
                    BrowserUtils.wait(3);
                    removeCustomerButton.click();
                    BrowserUtils.wait(3);

                    String oldWindow = Driver.get().getWindowHandle();
                    BrowserUtils.wait(2);
                    Set<String> windowHandles = Driver.get().getWindowHandles();

                    for (String windowHandle : windowHandles) {
                        if (!windowHandle.equals(oldWindow)) {
                            Driver.get().switchTo().window(windowHandle);
                        }
                    }
                    deleteButton.click();
                    BrowserUtils.wait(10);
                    System.out.println("delete " + count);

                    count++;

                    List<WebElement> dealersList1 = Driver.get().findElements(By.xpath("//*[@class='mat-cell cdk-column-name mat-column-name ng-star-inserted']"));
                    dealersList = dealersList1;

                } else {
                    System.out.println("There are not much more company which matches your search");
                    break;
                }
            }

        }}


