package pages;

import Utulities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RMA_Page extends BasePage{


    @FindBy(xpath = "//html//body//m-pages//div//div//div//ng-component//m-rma-index//div//div//h3//span[contains(text(),'RMA')]")
    public WebElement RMA_header ;

    @FindBy(xpath = "//button[contains(text(),'Reload')]")
    public WebElement reloadButton;

    @FindBy(xpath = "//span[contains(text(),'Filters')]")
    public WebElement filters;

    @FindBy(xpath = "//button[contains(text(),'Filter')]")
    public WebElement filterButton;

   @FindBy(xpath = " //span[contains(text(),'Reset')]")
    public WebElement reset;

   @FindBy(xpath ="//input[@name='poNumber']")
    public WebElement PO_Number;

   @FindBy(xpath = "//div//div//div//div//div//div[2]//div[1]//mat-form-field[1]//div[1]//div[1]//div[1]//input[1]")
    public WebElement DealerFilter;

   @FindBy(xpath = "//body/m-pages/div/div/div/ng-component/m-rma-index/div/div/m-rma-filters/div/form/div/div/div/mat-form-field/div/div/div/mat-select[@name='status']/div/div[1]")
    public WebElement AnyStatus;

   @FindBy(xpath = " //button[contains(text(),'#ID')]")
    public WebElement ID ;

   @FindBy(xpath = " //button[contains(text(),'Status')]")
    public WebElement Status;

   @FindBy(xpath = " //button[contains(text(),'Balance')]")
    public WebElement Balance;

   public RMA_Page(){
       PageFactory.initElements(Driver.get(), this);
   }




}
