package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

    public class ResourcesPage extends BasePage {

    public ResourcesPage(){
        PageFactory.initElements(Driver.get(),this);
    }

    @FindBy(xpath = "//button[@class='btn btn-primary ng-star-inserted']")
    public WebElement addResourcesButton;

    @FindBy(xpath = "(//*[starts-with(@id,\"mat-input\")])[2]")
    public WebElement labelBox;

    @FindBy(xpath = "(//*[starts-with(@id,\"mat-input\")])[3]")
    public WebElement tagBox;

    @FindBy(xpath = "//*[@type='file']")
    public WebElement chooseFileButton;

    public void clickAddResources(){
        addResourcesButton.click();
    }

    public void setLabel(String str){
    BrowserUtils.waitForStaleElement(labelBox);
     labelBox.sendKeys(str);
}

    public void clickTagBox(String str){
    BrowserUtils.waitForStaleElement(tagBox);
    tagBox.click();
    BrowserUtils.wait(1);
    List <WebElement> tagList = Driver.get().findElements(By.xpath("//*[@class='mat-option-text']"));
    for (WebElement each:tagList ) {
        if(each.getText().equals(str)){
            each.click();
            break;
        }
    }
}


}

