package pages;

import Utulities.BrowserUtils;
import Utulities.ConfigurationReader;
import Utulities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

public class BasePage {
    public BasePage() {

        PageFactory.initElements(Driver.get(), this);
    }

    // Dashboard and Styles module doesn't have pageHeader
    @FindBy(xpath = "//h3[contains(.,'')]")
    public WebElement pageHeader;

    @FindBy(xpath = "/html/head/title")
    public WebElement pageTitle;

    @FindBy(xpath = "//span[contains(text(),'Dashboard')]")
    public WebElement dashboard;

    @FindBy(xpath = "//span[contains(text(),'Dealers')]")
    public WebElement dealers;

    @FindBy(xpath = "//span[contains(text(),'Proposals')]")
    public WebElement proposals;

    @FindBy(xpath = "//span[contains(text(),'Orders')]")
    public WebElement orders;

    @FindBy(xpath = "//span[contains(text(),'RMA')]")
    public WebElement rma;

    @FindBy(xpath = "//span[contains(text(),'Styles')]")
    public WebElement styles;

    @FindBy(xpath = "//span[contains(text(),'Parts')]")
    public WebElement parts;

    @FindBy(xpath = "//span[contains(text(),'Inventory')]")
    public WebElement inventory;

    @FindBy(xpath = "//span[contains(text(),'Resources')]")
    public WebElement resources;

    @FindBy(xpath = "//span[contains(text(),'Feedback')]")
    public WebElement feedback;

    @FindBy(xpath = "//span[contains(text(),'Announcements')]")
    public WebElement announcements;

    @FindBy(xpath = "//span[contains(text(),'Accounting')]")
    public WebElement accounting;

    @FindBy(xpath = "//span[contains(text(),'Expenses')]")
    public WebElement expenses;

    @FindBy(xpath = "//span[contains(text(),'Reports')]")
    public WebElement reports;

    @FindBy(xpath = "//span[contains(text(),'Settings')]")
    public WebElement settings;

    @FindBy(id = "m_aside_left_minimize_toggle")
    public WebElement minimizeButton;

    @FindBy(xpath = "//*[contains(text(),'Term')]")
    public WebElement termsAndConditions;

    @FindBy(xpath = "//*[contains(text(),'About')]")
    public WebElement aboutButton;

    @FindBy(id = "company")
    public WebElement companyName;

    @FindBy(name = "intercom-launcher-frame")
    public WebElement interComeLauncher;

    @FindBy(xpath = "//*[contains( @class,'chmsnk')]")
    public WebElement interComeTitle;

    @FindBy(xpath = "//*[@type='file']")
    public WebElement chooseFileButton;

    // Dashboard and Styles module doesn't have pageHeader
    public String getPageHeader() {
        BrowserUtils.wait(5);
        String header = pageHeader.getText();
        return header;
    }

    public void navigateTo(String moduleName) {

//<<<<<<< HEAD

//=======
//>>>>>>> master
        String moduleLocator = "//span[contains(text(),'"+moduleName+"')]";

        WebDriverWait wait = new WebDriverWait(Driver.get(), 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(moduleLocator)));
        WebElement module = Driver.get().findElement(By.xpath(moduleLocator));
        BrowserUtils.wait(3);
        ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].scrollIntoView(true);", module);

        //   Actions actions = new Actions(Driver.get());
        //  actions.moveToElement(module);
        //  actions.perform();
        module.click();
//        if (ConfigurationReader.getProperty("browser").equalsIgnoreCase("chrome")) {
//            ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].scrollIntoView(true);", element);
//            element.click();
//        } else
//            element.click();}

    }


    public String getPageTitle() {
        BrowserUtils.waitForStaleElement(pageTitle);
        String title = Driver.get().getTitle();
        return title;
    }

    public String getCompanyName() {
        BrowserUtils.wait(3);
        String text = companyName.getText();
        return text;
    }

    public void clickInterComeLauncher() {
        BrowserUtils.wait(3);
        BrowserUtils.waitForStaleElement(interComeLauncher);
        interComeLauncher.click();
    }

    public String getInterComeLauncherTitle() {
        String title = interComeTitle.getText();
        return title;
    }

    public String getTotalNumberOfList() {
        WebElement number = Driver.get().findElement(By.xpath("//*[@class='mat-paginator-range-label']"));
        String totalPartNumber = number.getText();
        totalPartNumber = totalPartNumber.substring(totalPartNumber.lastIndexOf(" ") + 1);

        return totalPartNumber;
    }

    public String getPageHeader(String str) {
        WebElement headerLocator = Driver.get().findElement(By.xpath("//h3[contains(.,'" + str + "')]"));
        WebDriverWait wait = new WebDriverWait(Driver.get(), 15);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h3[contains(.,'" + str + "')]")));
        WebElement module = Driver.get().findElement(By.xpath("//h3[contains(.,'" + str + "')]"));
        String header2 = headerLocator.getText();
        return header2;

    }

    public void clickChooseFileButton() {
        BrowserUtils.wait(1);
        chooseFileButton.click();
    }

    // upload file from resources file
    public void uploadFileByClickChooseFileButton(String path) {

        String projectPath = System.getProperty("user.dir");
        String filePath = projectPath+"/"+path;

        BrowserUtils.waitForStaleElement(chooseFileButton);
        chooseFileButton.sendKeys(filePath);
        BrowserUtils.waitForPageToLoad(5);
    }

    public void navigateTo(String moduleName, String subModuleName) {

        String moduleLocator = "//span[contains(text(),'"+moduleName+"')]";

        WebDriverWait wait = new WebDriverWait(Driver.get(), 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(moduleLocator)));
        WebElement module = Driver.get().findElement(By.xpath(moduleLocator));
        BrowserUtils.wait(3);
        ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].scrollIntoView(true);", module);

        module.click();

        String subModuleLocator = "//span[contains(text(),'"+subModuleName+"')]";

        WebElement subModule = Driver.get().findElement(By.xpath(subModuleLocator));
        BrowserUtils.waitForStaleElement(subModule);
        subModule.click();
        BrowserUtils.wait(1);
        }

    public static void highLightElement(WebElement element) {

        try {
            JavascriptExecutor js = (JavascriptExecutor) Driver.get();
            js.executeScript("arguments[0].setAttribute('style','border: 2px solid red;');", element);
            Thread.sleep(1000);
            js.executeScript("arguments[0].style.border=''", element, "");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    }


