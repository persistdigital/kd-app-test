package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;


public class PartsPage extends BasePage {

    @FindBy(xpath = "(//*[@class='btn btn-primary margin-right ng-star-inserted'])[1]")
    public WebElement scalePricesButton;

    @FindBy(xpath = "(//*[starts-with(@id,\"mat-input\")])[2]")
    public WebElement scalePriceMultiplier;

    @FindBy(css = "[class='btn btn-primary']")
    public WebElement applyButton;

    @FindBy(xpath = "(//*[@class='btn btn-primary margin-right ng-star-inserted'])[2]")
    public WebElement csvFileUploadButton;

    @FindBy(xpath = "(//*[@type='button'])[3]")
    public WebElement addPartButton;

    @FindBy(css = "[class='btn btn-primary']")
    public WebElement uploadButton;

    @FindBy(xpath = "(//*[starts-with(@id,'mat-input')])[2]")
    public WebElement addPart_Code;

    @FindBy(xpath = "(//*[starts-with(@class,'mat-select-placeholder ng-tns')])[1]")
    public WebElement addPart_Type;

    @FindBy(xpath = "(//*[starts-with(@id,'mat-input')])[3]")
    public WebElement addPart_Description;

    @FindBy(xpath = "(//*[starts-with(@id,'mat-input')])[4]")
    public WebElement addPart_Synonyms;

    @FindBy(xpath = "(//*[starts-with(@id,'mat-input')])[5]")
    public WebElement addPart_Width;

    @FindBy(xpath = "(//*[starts-with(@id,'mat-input')])[6]")
    public WebElement addPart_Height;

    @FindBy(xpath = "(//*[starts-with(@id,'mat-input')])[7]")
    public WebElement addPart_Length;

    @FindBy(xpath = "//*[contains(text(),'Charge assembly')]")
    public WebElement addPart_chargeAssemblyCheckBox;

    @FindBy(xpath = "//*[contains(text(),'Apply discount')]")
    public WebElement addPart_applyDiscountCheckBox;

    @FindBy(xpath = "//*[contains(text(),'Global')]")
    public WebElement addPart_globalCheckBox;

    @FindBy(xpath = "(//*[starts-with(@id,'mat-input')])[8]")
    public WebElement addPart_piecesPerContainer;

    @FindBy(xpath = "//*[@class='mat-error ng-star-inserted']")
    public WebElement addPartCodeErrorMessage;

    @FindBy(xpath = "(//*[@class='mat-error ng-star-inserted'])[2]")
    public WebElement addPartDescriptionErrorMessage;

    public void clickCsvFileUploadButton() {
        csvFileUploadButton.click();
        BrowserUtils.wait(2);
    }



    public void clickUploadButton() {
        BrowserUtils.wait(1);
        uploadButton.click();
    }

    public void clickScalePrice() {
        BrowserUtils.wait(1);
        scalePricesButton.click();
    }

    public void setScalePriceMultiplier(String multiplier) {
        BrowserUtils.wait(1);
        scalePriceMultiplier.clear();
        BrowserUtils.wait(1);
        scalePriceMultiplier.sendKeys(multiplier);
    }

    public void clickApplyButton() {
        BrowserUtils.wait(1);
        applyButton.click();
    }

    public void clickAddPartButton() {
        addPartButton.click();
        BrowserUtils.wait(2);
    }

    public void addPartWriteCode(String code) {
        BrowserUtils.wait(1);
        addPart_Code.sendKeys(code);
    }

    public void addPartSelectType(String type) {
        BrowserUtils.wait(2);
        addPart_Type.click();
        BrowserUtils.wait(2);
        WebElement selectType = Driver.get().findElement(By.xpath("//*[starts-with(@class,'mat-option-text')][contains(text(),'" + type + "')]"));
        selectType.click();


    }

    public void addPartWriteDescription(String description) {
        BrowserUtils.wait(2);
        addPart_Description.sendKeys(description);

    }

    public void setDimensions(String width, String height, String length) {
        BrowserUtils.wait(1);
        addPart_Width.clear();
        addPart_Width.sendKeys(width);
        BrowserUtils.wait(1);
        addPart_Height.clear();
        addPart_Height.sendKeys(height);
        BrowserUtils.wait(1);
        addPart_Length.clear();
        addPart_Length.sendKeys(length);
    }


    public void setAddPartClickGlobalCheckbox(String string) {
        BrowserUtils.wait(1);
        if (string.equalsIgnoreCase("yes")) {
            addPart_globalCheckBox.click();
            System.out.println("user say yes for global");
        } else {
            System.out.println("user say no global");
        }
    }

    public void setAddPartClickChargeAssembly(String string) {
        BrowserUtils.wait(1);
        if (string.equalsIgnoreCase("yes")) {
            System.out.println("user say yes for charge assembly");
        } else {
            // it was selected by default so when we click it will be not selected
            addPart_chargeAssemblyCheckBox.click();
            System.out.println("user say no for charge assembly ");
        }

    }


    public void setAddPart_applyDiscountCheckBox(String string) {
        BrowserUtils.wait(1);
        if (string.equalsIgnoreCase("yes")) {
            System.out.println("User say yes for discount ");
        } else {
            // it was selected by default so when we click it will be not selected
            addPart_applyDiscountCheckBox.click();
            System.out.println("user say no for discount");
        }
    }


    public void setAddPart_piecesPerContainer(String string) {
        BrowserUtils.wait(1);
        addPart_piecesPerContainer.clear();
        addPart_piecesPerContainer.sendKeys(string);
    }

    public void getErrorMessage() {
        Assert.assertEquals("Code is required", addPartCodeErrorMessage.getText());
        System.out.println("Code error Message is: " + addPartCodeErrorMessage.getText());
        Assert.assertEquals("Description is required", addPartDescriptionErrorMessage.getText());
        System.out.println("Description error message is: " + addPartDescriptionErrorMessage.getText());

    }

    public void getAllPartList() {

        WebElement nextPageArrowSign = Driver.get().findElement(By.xpath("(//*[@class='mat-paginator-icon'])[3]"));
        WebElement number = Driver.get().findElement(By.xpath("//*[@class='mat-paginator-range-label']"));

        String totalPartNumber = number.getText();
        totalPartNumber = totalPartNumber.substring(10, 12);
        System.out.println("total item number is: " + totalPartNumber);
        BrowserUtils.wait(1);
        int totalPartNum = Integer.parseInt(totalPartNumber);
        int i = 0;
        int count =0;
        while(count< totalPartNum) {
        List <WebElement> list = Driver.get().findElements(By.xpath("//*[contains(@class,'mat-cell cdk-column-code')]"));

            System.out.println(list.get(i).getText());
            BrowserUtils.wait(1);
             i++;
             count++;
            if (i %10 == 0 && nextPageArrowSign.isDisplayed()) {
            nextPageArrowSign.click();
            BrowserUtils.wait(1);
            i=0;
            }

        }
    }


}