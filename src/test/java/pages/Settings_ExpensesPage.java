package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class Settings_ExpensesPage extends BasePage {

    @FindBy(xpath = "//h3[contains(.,'')]")
    public WebElement pageHeaderExpenses;

    @FindBy(xpath = "//input[@class='mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored']")
    public WebElement addCategory;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    public WebElement clickAddCategory;

    @FindBy(xpath = "//span[contains(text(),'TestCategory')]")
    public WebElement verifyAddedCategory;

    @FindBy(xpath = "//div[1]/span[1][@class='ng-star-inserted']")
    public WebElement clickOnX;

    @FindBy(xpath = "//li[16]/div[1]/ul[1]/li[7]/a[1]/span[1]")
    public WebElement clickOnExpenseSubmodule;

    @FindBy(xpath = "//div[contains(text(),'Vendors')]")
    public WebElement clickOnVendors;

    @FindBy(xpath = "//*[@class='mat-paginator-range-label']")
    public WebElement verifyTotalNumber;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    public WebElement clickOnAddVendor;

    @FindBy(xpath = "//mat-error[contains(text(),'Name is required')]")
    public WebElement errorMessageForCreateVendor;

    @FindBy(xpath = "//input[@placeholder='Enter vendor name']")
    public WebElement vendorNameInput;

    @FindBy(xpath = "//td[contains(text(),'TestVendor')]")
    public WebElement verifyCreatedVendor;

    @FindBy(xpath = "//input[@placeholder='Search by vendor']")
    public WebElement searchCreatedVendor;

    @FindBy(xpath = "//td[contains(text(),'TestVendor')]")
    public WebElement sortedCreatedVendor;

    @FindBy(xpath = "//mat-icon[@class='mat-icon notranslate material-icons mat-icon-no-color']")
    public WebElement sortedVendorsTreeDot;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    public WebElement saveButton;

    @FindBy(xpath = "//td[contains(text(),'TestVendorNameChanged')]")
    public WebElement verifyUpdatedVendor;

    @FindBy(xpath = "//span[contains(text(),'Delete vendor')]")
    public WebElement deleteVendorOption;




    public void setAddCategory(String str){
        addCategory.sendKeys(str);
        BrowserUtils.wait(1);
    }

    public void clickAddCategoryButton(){
        clickAddCategory.click();
        BrowserUtils.wait(1);
    }

    public void verifyAddedCategoryName(){
        highLightElement(verifyAddedCategory);
    }

    public void clickOnXToRemove(){
        clickOnX.click();
        BrowserUtils.wait(2);
    }

    public void clickOnExpenseSubmoduleOfSettings(){
        ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].scrollIntoView(true);", clickOnExpenseSubmodule);
        BrowserUtils.wait(1);
        clickOnExpenseSubmodule.click();
        BrowserUtils.wait(1);
    }

    public void setClickOnVendors(){
        clickOnVendors.click();
        BrowserUtils.wait(1);
        highLightElement(verifyTotalNumber);
    }

    public void clickOnAddVendorButton(){
        clickOnAddVendor.click();
        BrowserUtils.wait(1);
    }

    public void getErrorMessageForCreateVendor(String string) {

        if (string.equals("Name is required")) {
            Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            Assert.assertEquals(string, errorMessageForCreateVendor.getText());
            highLightElement(errorMessageForCreateVendor);
            System.out.println("Code error Message is: " + errorMessageForCreateVendor.getText());
        }else{
        }
    }

    public void setVendorNameInput(String str){
        BrowserUtils.wait(1);
        vendorNameInput.clear();
        vendorNameInput.sendKeys(str);
        BrowserUtils.wait(1);
    }

    public void setVerifyCreatedVendor(){
        BrowserUtils.wait(1);
        highLightElement(verifyCreatedVendor);
    }

    public void setSearchCreatedVendor(String str){
        searchCreatedVendor.sendKeys(str);
        BrowserUtils.wait(2);
    }

    public void verifySortedCreatedVendor(){
        highLightElement(sortedCreatedVendor);
    }

    public void clickSortedVendorsTreeDot(){
        sortedVendorsTreeDot.click();
        BrowserUtils.wait(1);
    }

    public void clickSaveButton(){
        BrowserUtils.wait(1);
        saveButton.click();
        BrowserUtils.wait(2);
    }

    public void highlightUpdatedVendor(){
        highLightElement(verifyUpdatedVendor);
    }

    public void clickDeleteVendorOption(){
        BrowserUtils.wait(1);
        deleteVendorOption.click();
        BrowserUtils.wait(1);
    }

    public void highlightHeader(){
        highLightElement(pageHeaderExpenses);
    }




}
