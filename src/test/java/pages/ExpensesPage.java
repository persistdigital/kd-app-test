package pages;

import Utulities.BrowserUtils;
import Utulities.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


import java.security.Key;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ExpensesPage extends BasePage {

    @FindBy(xpath = "//button[@class='btn btn-primary ng-star-inserted']")
    public WebElement clickAddExpenses;

    @FindBy(xpath = "(//input[starts-with(@id,'mat-input')])[5]")
    public WebElement vendorBox;

    @FindBy(xpath = "(//input[starts-with(@id,'mat-input')])[6]")
    public WebElement descriptionBox;

    @FindBy(xpath = "(//input[starts-with(@id,'mat-input')])[7]")
    public WebElement amountBox;

    @FindBy(xpath = "//span[starts-with(@class,'mat-select-placeholder')]")
    public WebElement categoryBox;


    @FindBy(xpath = "//tbody/tr[1]//*[@role='img']")
    public WebElement clickThreeDots;

    @FindBy(xpath = "//button[@class='mat-menu-item ng-star-inserted']")
    public WebElement clickOnDelete;

    @FindBy(xpath = "//span[contains(text(),'Delete')]")
    public WebElement clickOnDeleteButton;

    @FindBy(xpath = "//span[contains(text(),'Edit')]")
    public WebElement clickOnEdit;

    @FindBy(xpath = "//mat-error[contains(text(),'Vendor is required')]")
    public WebElement addExpenseVendorErrorMessage;

    @FindBy(xpath = "//mat-error[contains(text(),'Amount is required')]")
    public WebElement addExpenseAmountErrorMessage;

    @FindBy(xpath = "//mat-error[contains(text(),'Category is required')]")
    public WebElement addExpenseCategoryErrorMessage;

    @FindBy(xpath = "//i[contains(@class,'flaticon-cancel')]")
    public WebElement closeCreateExpenseWindow;

    @FindBy(xpath = "//input[@placeholder='Search by vendor']")
    public WebElement vendorBoxFilter;

    @FindBy(xpath = "//input[@placeholder='Search by category']")
    public WebElement categoryBoxFilter;

    @FindBy(xpath = "//input[@id='mat-input-2']")
    public WebElement startDateBoxFilter;

    @FindBy(xpath = "//input[@id='mat-input-3']")
    public WebElement endDateBoxFilter;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    public WebElement clickOnFilter;

    @FindBy(xpath = "//button[@class='btn btn-outline-primary']")
    public WebElement clickOnReset;




    public void clickAddExpensesButton() {
        BrowserUtils.wait(3);
        clickAddExpenses.click();
    }

    public void setVendor(String str) {
        BrowserUtils.waitForStaleElement(vendorBox);
        vendorBox.clear();
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        vendorBox.sendKeys(str);
    }

    public void setDescription(String str) {
        BrowserUtils.waitForStaleElement(descriptionBox);
        descriptionBox.clear();
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        descriptionBox.sendKeys(str);
    }

    public void setAmount(String str) {
        BrowserUtils.waitForStaleElement(amountBox);
        amountBox.clear();
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        amountBox.sendKeys(str);
    }


    public void clickCategoryBox(String str) {
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        categoryBox.click();
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        List<WebElement> CategoryList = Driver.get().findElements(By.xpath("//*[@class='mat-option-text']"));
        for (WebElement each : CategoryList) {
            if (each.getText().equals(str)) {
                each.click();
                break;
            }
        }


    }

//    public void setDate(String str){
//        BrowserUtils.waitForStaleElement(vendorBox);
//        dateBox.sendKeys(str);
//    }

    public void clickCreateButton(String create) {
        WebElement createButton = Driver.get().findElement(By.xpath("//button[contains(text(),'" + create + "')]"));
        createButton.click();
    }

    public void clickThreeDotsField() {
        BrowserUtils.wait(2);
        clickThreeDots.click();
    }

    public void clickOnDeleteField() {
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        clickOnDelete.click();
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public void clickOnExpenseDeleteButton() {
        BrowserUtils.wait(2);
        clickOnDeleteButton.click();
        BrowserUtils.wait(4);
    }

    public void clickOnEditField() {
        BrowserUtils.waitForStaleElement(clickOnEdit);
        clickOnEdit.click();

    }

    public void getErrorMessageForAddExpense(String string) {

        if (string.equals("Amount is required")) {
            Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            Assert.assertEquals(string, addExpenseAmountErrorMessage.getText());
            highLightElement(addExpenseAmountErrorMessage);
            System.out.println("Code error Message is: " + addExpenseAmountErrorMessage.getText());
        } else if(string.equals("Vendor is required")) {
            Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            Assert.assertEquals(string, addExpenseVendorErrorMessage.getText());
            highLightElement(addExpenseVendorErrorMessage);
            System.out.println("Code error Message is: " + addExpenseVendorErrorMessage.getText());
        }else{
            Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            Assert.assertEquals(string, addExpenseCategoryErrorMessage.getText());
            highLightElement(addExpenseCategoryErrorMessage);
            System.out.println("Code error Message is: " + addExpenseCategoryErrorMessage.getText());


        }

    }

    public void clickCloseCreateExpenseWindow() {

        closeCreateExpenseWindow.click();
    }


    public void setVendorBoxFilter(String str) {
        BrowserUtils.wait(2);
        vendorBoxFilter.sendKeys(str);

    }

    public void setCategoryBoxFilter(String str) {
        BrowserUtils.waitForStaleElement(categoryBoxFilter);
        categoryBoxFilter.sendKeys(str);

    }

    public void setStartDateBoxFilter(String str) {
        BrowserUtils.waitForStaleElement(startDateBoxFilter);
        startDateBoxFilter.sendKeys(str);

    }

    public void setEndDateBoxFilter(String str) {
        BrowserUtils.waitForStaleElement(endDateBoxFilter);
        endDateBoxFilter.sendKeys(str);

    }

    public void clickOnFilterButton() {
        clickOnFilter.click();
    }

    public void VerifyTheFilteredExpense(String verify) {
        BrowserUtils.wait(2);
        WebElement verifyExpense = Driver.get().findElement(By.xpath("//td[contains(text(),'" + verify + "')]"));
        BrowserUtils.wait(2);
        Assert.assertEquals(verify, verifyExpense.getText());
        highLightElement(verifyExpense);
        System.out.println("Verified expense is: " + verifyExpense.getText());

    }

    public void clickOnResetButton() {
        clickOnReset.click();
    }


//    public static void highLightElement(WebElement element) {
//
//        try {
//            JavascriptExecutor js = (JavascriptExecutor) Driver.get();
//            js.executeScript("arguments[0].setAttribute('style','border: 2px solid red;');", element);
//            Thread.sleep(1000);
//            js.executeScript("arguments[0].style.border=''", element, "");
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }


}