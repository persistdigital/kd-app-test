package pages;
import Utulities.BrowserUtils;
import Utulities.Driver;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class DealersPage extends BasePage {

    public DealersPage() {
            PageFactory.initElements(Driver.get(),this);
        }

     @FindBy(xpath = "//h3[contains(.,'Dealers')]")
      public WebElement pageHeader;

     @FindBy(css = "[class='btn btn-primary ng-star-inserted']")
     public WebElement AddDealerButton;

     @FindBy(css = "[class='btn btn-primary ng-star-inserted']")
     public WebElement CSVUploadButton;

     @FindBy(css = "[class='mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored']")
     public WebElement SearcDealersButton;

     @FindBy(css = "[class='mat-select-value']")
     public WebElement SalesRepButton;

     @FindBy(css = "[class='ng-tns-c13-60 ng-star-inserted']")
     public WebElement DisplayedColumnsButton;

     @FindBy(css = "[class='mat-checkbox-inner-container']")
     public WebElement PositiveBalanceButton;

     @FindBy(xpath = "//input[contains(.,'')]")
     public WebElement CompanyName;

     @FindBy(xpath = "//form//div[@class='row'][1]/div[2]//input")
     public WebElement CompanyEmail;

    @FindBy(xpath = "//form/div/div/div[2]/div[1]//input")
    public WebElement Address;

    @FindBy(xpath = "//form/div/div/div[3]/div[1]//input")
    public WebElement MainPhone;

    @FindBy(css = "[class='btn btn-primary']")
    public WebElement SubmitButton;

    @FindBy(css= "[class='mat-form-field-flex']")
    public WebElement companyBox;

    @FindBy(xpath = "/html[1]/body[1]/m-pages[1]/div[1]/div[1]/div[1]/ng-component[1]/m-dealers-create[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/mat-form-field[1]/div[1]/div[2]/div[1]/mat-error[1]")
    public WebElement companyNameErrorMessage;

    @FindBy(xpath = "/html[1]/body[1]/m-pages[1]/div[1]/div[1]/div[1]/ng-component[1]/m-dealers-create[1]/div[1]/form[1]/div[1]/div[1]/div[2]/div[1]/div[1]/mat-form-field[1]/div[1]/div[2]/div[1]/mat-error[1]")
    public  WebElement dealerAddressErrorMessage;

    @FindBy(xpath = "  /html[1]/body[1]/m-pages[1]/div[1]/div[1]/div[1]/ng-component[1]/m-dealers-create[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[2]/div[1]/mat-form-field[1]/div[1]/div[2]/div[1]/mat-error[1]")
    public WebElement dealerEmailErrorMessage;

    @FindBy(xpath = "/html[1]/body[1]/m-pages[1]/div[1]/div[1]/div[1]/ng-component[1]/m-dealers-create[1]/div[1]/form[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/mat-form-field[1]/div[1]/div[2]/div[1]/mat-error[1]")
    public WebElement dealerPhoneErrorMessage;

    @FindBy(xpath = "//form//div[@class='row']//mat-error")
    public List<String> warningMessages;

    @FindBy(xpath = "//*[contains(text(),'more_vert')]")
    public WebElement threeDot;  //satır sonundaki uc nokta
//   (//*[@role='img'])[3]

    @FindBy(xpath = "(//*[@role='menuitem'])[1]")
    public WebElement inviteUrl;

    @FindBy(xpath = "(//*[@role='menuitem'])[2]")
    public WebElement editDealer;

    @FindBy(xpath = "(//*[@role='menuitem'])[3]")
    public WebElement removeDealer;

    // //*[@class='m-portlet']/div[2]/div[2]/div/*[@class='mat-button mat-button-base mat-primary']
    // //*[@class='mat-button mat-button-base mat-primary']
    @FindBy(xpath = "//span[contains(text(),'Delete')]")  //  (//*[@class='mat-button-wrapper'])[16]
    public WebElement deleteButton;


public void AddDealer() {

    AddDealerButton.click();
    BrowserUtils.wait(2);

    CompanyName.sendKeys("NewDealer");
    BrowserUtils.wait(2);

   // CompanyEmail.click();
    CompanyEmail.sendKeys("kiymet1234@gmail.com");
    BrowserUtils.wait(2);

    CompanyEmail.click();
    Address.sendKeys("1600 Mahar Ave");
    BrowserUtils.wait(2);

    MainPhone.sendKeys("1231234564");
    BrowserUtils.wait(2);

    SubmitButton.click();
    BrowserUtils.wait(2);

}

    public void AddDealer(String companyName, String email, String address , String phone) {

        AddDealerButton.click();
        BrowserUtils.wait(2);

        CompanyName.sendKeys(companyName);
        BrowserUtils.wait(2);

        CompanyEmail.sendKeys(email);
        BrowserUtils.wait(2);

        CompanyEmail.click();
        Address.sendKeys(address);
        BrowserUtils.wait(2);

        MainPhone.sendKeys(phone);
        BrowserUtils.wait(2);

        SubmitButton.click();
        BrowserUtils.wait(2);
        System.out.println(companyName +" is created");

    }

    public void deleteDealer(String dealerName){

        SearcDealersButton.sendKeys(dealerName);
        BrowserUtils.wait(3);

        WebElement number = Driver.get().findElement(By.xpath("//*[@class='mat-paginator-range-label']"));
        String totalPartNumber = number.getText();

        System.out.println(totalPartNumber);
        totalPartNumber=totalPartNumber.substring(totalPartNumber.lastIndexOf(" ")+1);

        System.out.println("total dealers which you wanna delete number is: " + totalPartNumber);
        BrowserUtils.wait(2);
        int totalDealerNum = Integer.parseInt(totalPartNumber);

        int count = 0;
        while (count < totalDealerNum) {

    List <WebElement> dealersList = Driver.get().findElements(By.xpath("//*[@class='mat-cell cdk-column-name mat-column-name ng-star-inserted']"));

        if (dealersList.get(0).getText().contains(dealerName)) {


            JavascriptExecutor js = (JavascriptExecutor) Driver.get();
            js.executeScript("scroll(200, 500);");
            BrowserUtils.wait(2);
            threeDot.click();
        //    Actions actions = new Actions(Driver.get());
         //   actions.moveToElement(threeDot).click().build().perform();
            BrowserUtils.wait(5);
            removeDealer.click();
            BrowserUtils.wait(5);

            String oldWindow = Driver.get().getWindowHandle();
            BrowserUtils.wait(2);
            Set<String> windowHandles = Driver.get().getWindowHandles();

            for (String windowHandle : windowHandles) {
                if (!windowHandle.equals(oldWindow)) {
                    Driver.get().switchTo().window(windowHandle);
                }
            }
               deleteButton.click();
          BrowserUtils.wait(15);
            System.out.println( ++count +"  " +dealerName+ " was deleted ");



    List<WebElement> dealersList1 = Driver.get().findElements(By.xpath("//*[@class='mat-cell cdk-column-name mat-column-name ng-star-inserted']"));
    dealersList = dealersList1;

            } else{
            System.out.println("There are not much more company which matches your search");
            break;
        }
        }

    }

    // yeni dealer create ediyor ve return olarak da  onun adını veriyor
    public String AddNewDealerGetItsName() {
        Locale local =new Locale("en-US");
        Faker faker = new Faker(local);

        AddDealerButton.click();
        BrowserUtils.wait(2);

        String companyName = faker.name().firstName();
        String companyEmailAddress = faker.internet().emailAddress();
        String companyAddress = faker.address().fullAddress();
        String companyPhoneNumber =faker.phoneNumber().phoneNumber();

        CompanyName.sendKeys(companyName);
        BrowserUtils.wait(2);

        CompanyEmail.sendKeys(companyEmailAddress);
        BrowserUtils.wait(2);

        CompanyEmail.click();
        Address.sendKeys(companyAddress);
        BrowserUtils.wait(2);

        MainPhone.sendKeys(companyPhoneNumber);
        BrowserUtils.wait(2);

        SubmitButton.click();
        BrowserUtils.wait(2);
        System.out.println(companyName +" is created");

        BrowserUtils.wait(3);
        return companyName;
    }

    }


