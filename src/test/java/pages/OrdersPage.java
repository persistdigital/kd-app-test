package pages;

import Utulities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class OrdersPage extends BasePage {
    @FindBy(xpath = "//h3[@class=\"float-left\"]/span")
    public WebElement pageTitle;
    @FindBy(xpath = "//tbody/tr[1]//i")
    public WebElement edit;
    @FindBy(xpath = "//*[@class=\"details\"]/div[2]/div[1]/div[2]")
    public WebElement totalPrice;
    @FindBy(xpath = "//*[@class=\"page-body\"]//div//form/div[6]/span")
    public WebElement outstanding;
    @FindBy(xpath = "//*[@class=\"version-items\"]//tr[7]/td[1]")
    public WebElement rowNum;
    @FindBy(xpath = "//*[@class=\"total-items ng-star-inserted\"]/h5")
    public WebElement totalNumOfItems;
    @FindBy(xpath = "//form//div[@class=\"row\"][2]/div[1]")
    public WebElement orderStatus1;
    @FindBy(xpath = "//table//tr[2]/td[4]")
    public WebElement orderStatus2;
    @FindBy(xpath = "//*[contains(text(),'Delivery/Pickup date provided')]")
    public List<WebElement> verifyOrderStatus;
    @FindBy(xpath = "//form/div[2]/div[1]//*[@class=\"mat-select-value\"]")
    public WebElement currentOrderStatus;


    public void navigateToOrderPage(String order) {
        String module = "//ul//span[contains(text(),'" + order + "')]";
        //ul//span[contains(text(),'Order')]
        Driver.get().findElement(By.xpath(module)).click();
    }

    public List<WebElement> setOrderStatus() {
        String orderStatus = currentOrderStatus.getText();
//        System.out.println(orderStatus);
        List<WebElement> os = Driver.get().findElements(By.xpath("//*[contains(text(),'" + orderStatus + "')]"));
        return os;
    }

}
