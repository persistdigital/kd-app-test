package StepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.FeedbackPage;

public class FeedbackStepDefinitions {
    FeedbackPage feedbackPage=new FeedbackPage();

    @And("user selects {string}")
    public void userSelects(String dealer) {
        feedbackPage.selectDealer(dealer);
    }

    @Then("verify that message {string} is displayed")
    public void verifyThatMessageIsDisplayed(String message) {
        Assert.assertEquals(message,feedbackPage.actualMessage(message));

    }



}
