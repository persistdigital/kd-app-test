package StepDefinitions;

import Utulities.BrowserUtils;
import Utulities.ConfigurationReader;
import Utulities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.BasePage;
import pages.LoginPage;
import java.util.Set;

public class LoginStepDefinitions {
    BasePage basePage = new BasePage();
    LoginPage loginPage = new LoginPage();


    @Given("user is on the login page")
    public void user_is_on_the_login_page() {
        System.out.println("I am on the login page");
        Driver.get().get(ConfigurationReader.getProperty("url"));
    }

    @Then("user logs in with valid credentials")
    public void user_logs_in_with_valid_credentials() {


        System.out.println("Login with valid credentials");
        String email = ConfigurationReader.getProperty("email");
        String password = ConfigurationReader.getProperty("password");
        BrowserUtils.waitForPageToLoad(6);
        loginPage.login(email, password);
    }

    @Then("user verifies that {string} companyname is displayed")
    public void user_verifies_that_companyname_is_displayed(String string) {

        BrowserUtils.wait(3);
        Assert.assertEquals("KitchenDEV Cabinet Pricing and Ordering Software", basePage.getPageTitle());
        System.out.println("Verifying page title: " + basePage.getPageTitle());

        //  Assert.assertEquals("leye",basePage.getCompanyName());
        System.out.println("Company name is " + basePage.getCompanyName());
    }

    @Then("user logs in as {string}")
    public void user_logs_in_as(String role) {
        loginPage.login(role);
        BrowserUtils.wait(1);
        System.out.println("I login as " + role);
    }

    @Then("user click InterComeLauncher")
    public void user_click_InterComeLauncher() {
        basePage.clickInterComeLauncher();
    }

    @Then("user verifies that company name is displayed on launcher")
    public void user_verifies_that_company_name_is_displayed_on_launcher() {
        Driver.get().switchTo().frame("intercom-messenger-frame");
        BrowserUtils.wait(5);
        System.out.println(basePage.getInterComeLauncherTitle());

    }

    @Then("user verifies that {string} page subtitle is displayed")
    public void user_verifies_that_page_subtitle_is_displayed(String string) {
        BrowserUtils.wait(3);
        System.out.println("Verifying page title: " + basePage.getPageTitle());
    }

    @Then("user enters {string} email and {string} password")
    public void user_enters_email_and_password(String string, String string2) {
        String email = string;
        String password = string2;
        loginPage.login(email, password);
        BrowserUtils.wait(2);
        loginPage.loginButton.click();

    }

    @Then("user verify that page title is {string}")
    public void user_verify_that_page_title_is(String string) {
        String oldWindow = Driver.get().getWindowHandle();
        BrowserUtils.wait(2);

        Set<String> windowHandles = Driver.get().getWindowHandles();

        for (String windowHandle : windowHandles) {
            if (!windowHandle.equals(oldWindow)) {
                Driver.get().switchTo().window(windowHandle);
            }
        }
        System.out.println("Page title is: " + basePage.getPageTitle());
        Assert.assertEquals(string, basePage.getPageTitle());

    }

    @Then("user click About button")
    public void user_click_About_button() {
        loginPage.aboutButton.click();
    }

    @Then("user click Contact button")
    public void user_click_Contact_button() {
        loginPage.contactButton.click();

    }

    @Given("user logs in as {string} and on the home page")
    public void user_logs_in_as_and_on_the_home_page(String string) {
        Driver.get().get(ConfigurationReader.getProperty("url"));
        loginPage.login(string);
    }

    @Then("user click forgotPassword button")
    public void user_click_forgotPassword_button() {
        BrowserUtils.wait(1);
        loginPage.forgotPassword.click();
    }

    @Then("user verify that {string} label is display")
    public void user_verify_that_label_is_display(String string) {
        Assert.assertEquals(string, loginPage.enterYourEmail());
    }


}

