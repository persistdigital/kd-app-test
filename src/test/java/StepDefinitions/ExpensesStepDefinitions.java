package StepDefinitions;

import Utulities.Driver;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.BasePage;
import pages.ExpensesPage;

public class ExpensesStepDefinitions {

    ExpensesPage expensesPage = new ExpensesPage();


    @Then("user click addExpense button")
    public void user_click_addExpense_button() {
        expensesPage.clickAddExpensesButton();

    }

    @Then("user give {string} as vendor")
    public void user_give_as_vendor(String string) {
          expensesPage.setVendor(string);

    }

    @Then("user give {string} as description")
    public void user_give_as_description(String string) {
           expensesPage.setDescription(string);

    }

    @Then("user give {string} as amount")
    public void user_give_as_amount(String string) {
         expensesPage.setAmount(string);

    }

    @Then("user give {string} as category")
    public void user_give_as_category(String string) {
         expensesPage.clickCategoryBox(string);

    }

    @Then("user click {string} button")
    public void userClickButton(String create) {
          expensesPage.clickCreateButton(create);

    }

    @Then("user click on the three dots next to the newly created expense")
    public void userClickOnTheThreeDotsNextToTheNewlyCreatedExpense() {
        expensesPage.clickThreeDotsField();

    }

    @Then("user click on delete option")
    public void userClickOnDeleteOption() {
        expensesPage.clickOnDeleteField();
    }

    @Then("user click on expense delete button")
    public void userClickOnExpenseDeleteButton() {
        expensesPage.clickOnExpenseDeleteButton();
    }

    @Then("user click on edit option")
    public void userClickOnEditOption() {
        expensesPage.clickOnEditField();
    }

    @Then("user verify that alert {string} is displayed")
    public void userVerifyThatAlertIsDisplayed(String string) {
        expensesPage.getErrorMessageForAddExpense(string);

    }

    @Then("user click x to close Create Expense window")
    public void userClickXToCloseCreateExpenseWindow() {
        expensesPage.clickCloseCreateExpenseWindow();
    }

    @Then("user filter {string} as vendor")
    public void userFilterAsVendor(String string) {
        expensesPage.setVendorBoxFilter(string);
    }

    @Then("user filter {string} as category")
    public void userFilterAsCategory(String string) {
        expensesPage.setCategoryBoxFilter(string);
    }

    @Then("user filter {string} as start date")
    public void userFilterAsStartDate(String string) {
        expensesPage.setStartDateBoxFilter(string);
    }

    @Then("user filter {string} as end date")
    public void userFilterAsEndDate(String string) {
        expensesPage.setEndDateBoxFilter(string);
    }

    @Then("user click filter button")
    public void userClickFilterButton() {
        expensesPage.clickOnFilterButton();
    }

    @Then("user verify that filtered expense's {string} displayed")
    public void userVerifyThatFilteredExpenseSDisplayed(String string) {
        expensesPage.VerifyTheFilteredExpense(string);
    }

    @Then("user click reset button to cancel filter")
    public void userClickResetButtonToCancelFilter() {
        expensesPage.clickOnResetButton();
    }

//    @Then("user give {string} as date")
//    public void user_give_as_date(String string) {
//          expensesPage.setDate(string);
//
//    }





     }