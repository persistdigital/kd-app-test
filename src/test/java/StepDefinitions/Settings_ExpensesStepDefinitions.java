package StepDefinitions;

import io.cucumber.java.en.Then;
import pages.BasePage;
import pages.Settings_ExpensesPage;

public class Settings_ExpensesStepDefinitions {

    Settings_ExpensesPage settings_expensesPage = new Settings_ExpensesPage();
    BasePage basePage = new BasePage();

    @Then("user give {string} as category name")
    public void user_give_as_category_name(String string) {
        settings_expensesPage.setAddCategory(string);
    }

    @Then("user click add button")
    public void user_click_add_button() {
        settings_expensesPage.clickAddCategoryButton();
    }

    @Then("user verify that category created")
    public void user_verify_that_category_created() {
        settings_expensesPage.verifyAddedCategoryName();
    }

    @Then("user click X to remove created category")
    public void user_click_X_to_remove_created_category() {
        settings_expensesPage.clickOnXToRemove();
    }

    @Then("user click Vendors")
    public void user_click_Vendors() {
        settings_expensesPage.setClickOnVendors();
    }

    @Then("user click Add Vendor button")
    public void user_click_Add_Vendor_button() {
        settings_expensesPage. clickOnAddVendorButton();
    }

    @Then("user give {string} as Vendor name")
    public void user_give_as_Vendor_name(String string) {
        settings_expensesPage.setVendorNameInput(string);
    }

    @Then("user verify that Vendor created")
    public void user_verify_that_Vendor_created() {
        settings_expensesPage.setVerifyCreatedVendor();
    }

    @Then("user search {string} created Vendor")
    public void userSearchCreatedVendor(String string) {
        settings_expensesPage.setSearchCreatedVendor(string);
    }

    @Then("user verify that searched Vendor displayed")
    public void user_verify_that_searched_Vendor_displayed() {
        settings_expensesPage.verifySortedCreatedVendor();
    }

    @Then("user click on the three dots next to displayed Vendor")
    public void user_click_on_the_three_dots_next_to_displayed_Vendor() {
        settings_expensesPage.clickSortedVendorsTreeDot();
    }

    @Then("user click on save button")
    public void user_click_on_save_button() {
        settings_expensesPage.clickSaveButton();
    }

    @Then("user verify that Vendor updated")
    public void user_verify_that_Vendor_updated() {
        settings_expensesPage.highlightUpdatedVendor();
    }

    @Then("user click on delete vendor option")
    public void user_click_on_delete_vendor_option() {
        settings_expensesPage.clickDeleteVendorOption();
    }


    @Then("user click on Expense submodule")
    public void userClickOnExpenseSubmodule() {
        settings_expensesPage.clickOnExpenseSubmoduleOfSettings();
    }

    @Then("user verify that alert {string} is displayed for Create Vendor")
    public void userVerifyThatAlertIsDisplayedForCreateVendor(String string) {
           settings_expensesPage.getErrorMessageForCreateVendor(string);
    }


    @Then("user highlight page header")
    public void userHighlightPageHeader() {
        settings_expensesPage.highlightHeader();
    }
}
