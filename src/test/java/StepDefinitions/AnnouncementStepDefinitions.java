package StepDefinitions;

import io.cucumber.java.en.Then;
import pages.AnnouncementPage;

public class AnnouncementStepDefinitions {

    AnnouncementPage announcementPage = new AnnouncementPage();

    @Then("user click newAnnouncement button")
    public void user_click_newAnnouncement_button() {
        announcementPage.createNewAnnouncement();
    }

    @Then("user give {string} as title")
    public void user_give_as_title(String string) {
        announcementPage.setTitleBox(string);
    }

    @Then("user give {string} as content")
    public void user_give_as_content(String string) {
        announcementPage.setContentBox(string);

    }

    @Then("user writes {string} search button")
    public void user_writes_search_button(String string) {
        announcementPage.setSearchButton(string);
    }

    @Then("user click action button")
    public void user_click_action_button() {
      announcementPage.setThreedot();
    }

    @Then("user click Delete Announcement Button")
    public void user_click_Delete_Announcement_Button() {
        announcementPage.setDeleteAnnouncement();
    }

    @Then("user click delete button")
    public void user_click_delete_button() {
        announcementPage.setDeleteButton();
    }

    @Then("add Announcement and delete Announcement")
    public void add_Announcement_and_delete_Announcement() {
    announcementPage.addDeleteAnnouncement();
    }



}