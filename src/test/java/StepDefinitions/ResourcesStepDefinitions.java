package StepDefinitions;

import io.cucumber.java.en.Then;
import pages.BasePage;
import pages.ResourcesPage;

public class ResourcesStepDefinitions {

     ResourcesPage resourcesPage = new ResourcesPage();
     BasePage basePage = new BasePage();

    @Then("user click addResource button")
    public void user_click_addResource_button() {
      resourcesPage.clickAddResources();
    }

    @Then("user give {string} as label")
    public void user_give_as_label(String string) {
       resourcesPage.setLabel(string);
    }

    @Then("user give {string} as tag")
    public void user_give_as_tag(String string) {
       resourcesPage.clickTagBox(string);
    }

    @Then("user set file path {string} for upload")
    public void user_set_file_path_for_upload(String string) {
    resourcesPage.uploadFileByClickChooseFileButton(string);

    }

}
