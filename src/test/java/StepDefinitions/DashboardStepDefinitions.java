package StepDefinitions;

import Utulities.BrowserUtils;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.DashboardPage;


public class DashboardStepDefinitions {
    DashboardPage dashboardPage = new DashboardPage();

    @Then("user is on the dashboard page")
    public void user_is_on_the_dashboard_page() {
        System.out.println("I am on dashboard page");

    }

    @Then("verify that {string} {string} {string} buttons on the page")
    public void verify_that_buttons_on_the_page(String activeOrders, String dealers, String activeRMAs) {
        BrowserUtils.waitForVisibility(dashboardPage.activeOrders, 10);
        Assert.assertEquals(activeOrders, dashboardPage.activeOrders.getText());
        Assert.assertEquals(dealers, dashboardPage.dealers.getText());
        Assert.assertEquals(activeRMAs, dashboardPage.activeRMAs.getText());
    }

    @And("verify that {string} {string} on the page")
    public void verifyThatOnThePage(String arg0, String arg1) {


    }

    @And("verify that order status on the page")
    public void verifyThatOrderStatusOnThePage() {


    }
}
