package StepDefinitions;

import Utulities.Driver;
import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BasePage;
import pages.EditOrderPage;
import pages.OrdersPage;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class OrdersStepDefinitions {
    OrdersPage ordersPage = new OrdersPage();
    EditOrderPage editOrderPage = new EditOrderPage();
    BasePage basePage=new BasePage();


    @Then("user navigate to {string} page")
    public void user_navigate_to_page(String string) {
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        basePage.navigateTo(string);
        System.out.println("I am on "+string+" Page");
    }

    @Then("verify that {string} page subtitle is displayed")
    public void verify_that_page_subtitle_is_displayed(String expected)  {
        Driver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        Thread.sleep(3000);
        Assert.assertEquals(expected, ordersPage.pageTitle.getText());
        System.out.println(ordersPage.pageTitle.getText());
    }

    @Then("user navigates to Edit Order page")
    public void userNavigatesToEditOrderPage() {
        ordersPage.edit.click();
    }

    @And("verify that  Edit Order page subtitle is displayed")
    public void verifyThatEditOrderPageSubtitleIsDisplayed() {
        System.out.println("I am on Edit Page");
        Assert.assertEquals("Edit Order", editOrderPage.pageTitle.getText());
    }

    @And("verify that Outstanding and Total price are equal")
    public void verifyThatOutstandingAndTotalPriceAreEqual() {
        System.out.println(ordersPage.outstanding.getText());
        System.out.println(ordersPage.totalPrice.getText());
        Assert.assertTrue(ordersPage.outstanding.getText().contains(ordersPage.totalPrice.getText()));
    }

    @And("verify that row number and number of items are equal")
    public void verifyThatRowNumberAndNumberOfItemsAreEqual() {
        String rowNumber = ordersPage.rowNum.getText();
        System.out.println(rowNumber.substring(0, 1));
        System.out.println(ordersPage.totalNumOfItems.getText());
        Assert.assertTrue(ordersPage.totalNumOfItems.getText().contains(rowNumber.substring(0, 1)));
    }

    @And("verify order status same in order status and in table")
    public void verifyOrderStatusSameInOrderStatusAndInTable() {
        List<WebElement> listOrderStatus = ordersPage.setOrderStatus();
//        System.out.println(listOrderStatus.size());
        for (WebElement each : listOrderStatus) {
            System.out.println(each.getText());
        }
        Assert.assertEquals(listOrderStatus.get(0).getText(), listOrderStatus.get(1).getText());
    }

    @Then("verify that {string} page title is displayed")
    public void verifyThatPageTitleIsDisplayed(String header) {
        WebDriverWait wait=new WebDriverWait(Driver.get(),20);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h3[contains(.,'"+header+"')]")));
        Assert.assertEquals(header, basePage.getPageHeader(header));
    }
}
