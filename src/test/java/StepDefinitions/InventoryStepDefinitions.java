package StepDefinitions;

import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.InventoryPage;

import javax.sound.midi.Soundbank;

public class InventoryStepDefinitions {

    InventoryPage inventoryPage = new InventoryPage();

    @Then("user navigate to {string} page {string} submodule")
    public void user_navigate_to_page_submodule(String string, String string2) {
    inventoryPage.navigateTo(string,string2);
    }

    @Then("user verify that total item at the {string} module is{string}")
    public void user_verify_that_total_item_at_the_module_is(String string, String string2) {
        Assert.assertEquals("",inventoryPage.getTotalNumberOfList(),string2);
        System.out.println(string+ " module total number of item is: "+inventoryPage.getTotalNumberOfList());
    }

}