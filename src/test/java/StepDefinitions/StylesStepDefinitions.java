package StepDefinitions;

import Utulities.BrowserUtils;
import Utulities.Driver;
import io.cucumber.java.en.*;
import org.junit.Assert;
import pages.StylesPage;

public class StylesStepDefinitions {
    StylesPage stylesPage = new StylesPage();

    @Then("user click add Style button")
    public void user_click_add_Style_button() {

       stylesPage.clickCreateNewSeriesIcon();

    }

    @Then("user write {string} as Style name")
    public void user_write_as_Style_name(String string) {
       BrowserUtils.wait(2);
        stylesPage.createNewSeries(string);
    }

    @Then("user click create button")
    public void user_click_create_button() {
        BrowserUtils.wait(1);
        stylesPage.clickCreateButton();
        BrowserUtils.wait(2);
    }

    @Then("user click add Collection button at the {string} style")
    public void user_click_add_Collection_button_at_the_style(String string) {
        stylesPage.clickCreateCollectionButton(string);
        BrowserUtils.wait(2);
        Driver.get().switchTo().alert().accept();
        BrowserUtils.wait(2);
    }

    @Then("user write {string} as collection name")
    public void user_write_as_collection_name(String string) {
        stylesPage.giveCollectionName(string);
    }

    @Then("user write {string} as collection short name")
    public void user_write_as_collection_short_name(String string) {
       stylesPage.giveCollectionShortName(string);
    }

    @Then("user write {string} as price group")
    public void user_write_as_price_group(String string) {
        stylesPage.givePriceGroup(string);
    }

    @Then("verify that error messages are displayed")
    public void verify_that_error_messages_are_displayed() {

        BrowserUtils.wait(2);
        Driver.get().switchTo().alert().accept();
        BrowserUtils.wait(2);
        Assert.assertEquals("Name is required",stylesPage.nameRequiredErrorMessage.getText());
        Assert.assertEquals("Short Name is required",stylesPage.shortNameRequiredErrorMessage.getText());
        Assert.assertEquals("Price Group is required",stylesPage.priceGroupRequiredErrorMessage.getText());

    }






}
