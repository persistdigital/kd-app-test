package StepDefinitions;


import io.cucumber.java.en.*;
import pages.BasePage;
import pages.DealersPage;
import java.util.List;

public class DealersStepDefinitions {

    DealersPage dealersPage = new DealersPage();
    BasePage basePage = new BasePage();

   @Then("user Add dealer")
    public void user_Add_dealer() {
        dealersPage.AddDealer();
    }

    @Then("user click add dealers button")
    public void user_click_add_dealers_button() {
        dealersPage.AddDealerButton.click();
    }

    @Then("user click submit button")
    public void user_click_submit_button() {
        dealersPage.SubmitButton.click();
    }

/*
    @Then("user verify that {string},{string},{string},{string} messages are displayed")
    public void user_verify_that_message_is_displayed(String companyNameErrorMessage, String dealerAddressErrorMessage, String dealerEmailErrorMessage, String dealerPhoneErrorMessage) {


        BrowserUtils.waitForVisibility(dealersPage.companyNameErrorMessage, 10);
        Assert.assertEquals(companyNameErrorMessage, dealersPage.companyNameErrorMessage.getText());

        BrowserUtils.waitForVisibility(dealersPage.dealerAddressErrorMessage, 10);
        Assert.assertEquals(dealerAddressErrorMessage, dealersPage.dealerAddressErrorMessage.getText());

        BrowserUtils.waitForVisibility(dealersPage.dealerEmailErrorMessage, 10);
        Assert.assertEquals(dealerEmailErrorMessage, dealersPage.dealerEmailErrorMessage.getText());

        BrowserUtils.waitForVisibility(dealersPage.dealerPhoneErrorMessage, 10);
        Assert.assertEquals(dealerPhoneErrorMessage, dealersPage.dealerPhoneErrorMessage.getText());

        System.out.println(dealersPage.companyNameErrorMessage.getText() + " " + dealersPage.dealerAddressErrorMessage.getText() + " " +
                dealersPage.dealerEmailErrorMessage.getText() + " " + dealersPage.dealerPhoneErrorMessage.getText());
    }
    */


 @Then("user verify that warning messages are displayed")
public void user_verify_that_warning_messages_are_displayed(List<String> dataTable) {
     List<String>list = dealersPage.warningMessages;
     for ( String each:list)
           {
         System.out.println(each);
     }

 //Assert.assertEquals(dealersPage.warningMessages,dataTable);
    }
    @Then("user Add dealer {string},{string},{string},{string}")
    public void user_Add_dealer(String string, String string2, String string3, String string4) {
        dealersPage.AddDealer(string, string2, string3, string4);
    }

    @Then("user delete dealers {string}")
    public void user_delete_dealers(String string) {
        dealersPage.deleteDealer(string);
    }

    @Then("user delete dealers")
    public void user_delete_dealers() {
        //   dealersPage.deleteDealer();
}

    @Then("user add and delete dealer for smoke test")
    public void user_add_and_delete_dealer_for_smoke_test() {
        String dealerNameWhichWillDelete=dealersPage.AddNewDealerGetItsName();
        dealersPage.deleteDealer(dealerNameWhichWillDelete);
    }

}
