package StepDefinitions;

import Utulities.BrowserUtils;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.BasePage;
import pages.ProposalPage;

public class ProposalsStepDefinitions {

    ProposalPage proposalPage = new ProposalPage();
    BasePage basePage = new BasePage();

    @Then("user navigates to {string}")
    public void user_navigates_to(String string) {
        basePage.navigateTo(string);
       BrowserUtils.wait(1);

    }

    @Then("user verify that page header is {string}")
    public void user_verify_that_page_header_is(String string) {
        System.out.println(basePage.getPageHeader());
        Assert.assertEquals(string,basePage.getPageHeader());
        System.out.println("user is at the "+ string + " page");
    }

    @Then("user create proposal")
    public void user_create_proposal() {
        proposalPage.createProposal("company1","Winter (DG)","version 1","VAB48");
    }

    @Then("user create proposal with this info {string},{string},{string},{string}")
    public void user_create_proposal_with_this_info(String companyName, String collectionName, String version, String part) {
       proposalPage.createProposal(companyName, collectionName, version, part);
    }

    @Then("user click create proposal button")
    public void user_click_create_proposal_button() {
       proposalPage.createProposalButton.click();
        BrowserUtils.wait(2);
    }

    @Then("user click next button")
    public void user_click_next_button() {
      proposalPage.nextButton.click();
        BrowserUtils.wait(6);
    }


    @Then("user fill the form for {string} and click next button")
    public void user_fill_the_form_for_and_click_next_button(String string) {
        proposalPage.createProposal_Step1(string);
    }

    @Then("user verifies that {string} message is displayed")
    public void user_verifies_that_message_is_displayed(String string) {
        BrowserUtils.wait(1);
        Assert.assertEquals(string,proposalPage.getWarningMessage(string));
        System.out.println(proposalPage.getWarningMessage(string));
    }


    @Then("user delete proposals {string}")
    public void user_delete_proposals(String string) {
       proposalPage.deleteProposal(string);
    }

    @Then("user delete proposals by {string} ID number")
    public void user_delete_proposals_by_ID_number(String string) {
       proposalPage.deleteProposalByProposalID(string);
    }

    @Then("user get total number")
    public void user_get_total_number(){
        System.out.println("Total number of List: "+ basePage.getTotalNumberOfList());
    }

}
