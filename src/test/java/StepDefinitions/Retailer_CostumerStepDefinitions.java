package StepDefinitions;

import io.cucumber.java.en.*;
import pages.BasePage;
import pages.Retailer_customersPage;


public class Retailer_CostumerStepDefinitions {

    Retailer_customersPage retailer_customersPage = new Retailer_customersPage();
    BasePage basePage = new BasePage();

    @Then("user add new customer as {string} name")
    public void user_add_new_customer_as_name(String string) {
        retailer_customersPage.clickNewCustomerButton();
        retailer_customersPage.setCustomerName(string);
        retailer_customersPage.clickSubmitButton();

    }

    @Then("user delete customer {string}")
    public void user_delete_customer(String string) {
       retailer_customersPage.deleteCustomers(string);
    }


    }


