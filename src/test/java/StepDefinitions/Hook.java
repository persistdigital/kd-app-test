package StepDefinitions;

import Utulities.ConfigurationReader;
import Utulities.Driver;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.util.concurrent.TimeUnit;


public class Hook {

    @Before
    public void setup() {

        String browser = ConfigurationReader.getProperty("browser");
        if (!browser.contains("remote") && !browser.contains("mobile")) {

            Driver.get().manage().window().maximize();
        }
        // Driver.get().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


    }

    @After
    public void teardown(Scenario scenario) {
        if(scenario.isFailed()){
            System.out.println("Test failed!");
            byte[] screenshot = ((TakesScreenshot)Driver.get()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }else{
            System.out.println("Cleanup!");
            System.out.println("Test completed!");
//        after every test, we gonna close browser
            Driver.close();
        }
    }
}