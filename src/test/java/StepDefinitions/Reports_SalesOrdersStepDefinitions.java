package StepDefinitions;

import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.BasePage;
import pages.Reports_SalesOrdersPage;

public class Reports_SalesOrdersStepDefinitions   {

    Reports_SalesOrdersPage reports_SalesOrdersPage = new Reports_SalesOrdersPage();
    BasePage basePage = new BasePage();

    @Then("user give {string} as start date")
    public void user_give_as_start_date(String string) {
        reports_SalesOrdersPage.setStartDateBox(string);
    }

    @Then("user give {string} as end date")
    public void user_give_as_end_date(String string) {
        reports_SalesOrdersPage.setEndDateBox(string);
    }

    @Then("user give {string} as dealer name")
    public void user_give_as_dealer_name(String string) {
        reports_SalesOrdersPage.setDealerBox(string);
    }

    @Then("user verify that filtered dealer sales order {string} displayed")
    public void userVerifyThatFilteredDealerSalesOrderDisplayed(String verify) {
        reports_SalesOrdersPage.VerifyTheFilteredDealer(verify);
    }

    @Then("user click send email button")
    public void user_click_send_email_button() {
        reports_SalesOrdersPage.clickOnSendEmailButton();
    }

    @Then("user click send button")
    public void user_click_send_button() {
        reports_SalesOrdersPage.clickOnSendButton();
    }

    @Then("user verify that filtered sales orders total amounts are correct")
    public void user_verify_that_filtered_sales_orders_total_amounts_are_correct() {
        reports_SalesOrdersPage.verifyTotalBalanceAmountSum();
    }





}
