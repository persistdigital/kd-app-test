package StepDefinitions;


import io.cucumber.java.en.Then;
import pages.BasePage;
import pages.Settings_TaxesPage;


public class Settings_TaxesStepDefinitions {

    Settings_TaxesPage settings_TaxesPage = new Settings_TaxesPage();
    BasePage basePage = new BasePage();


    @Then("user give {string} as Label")
    public void user_give_as_Label(String string) {
        settings_TaxesPage.setTaxLabelInput(string);
    }

    @Then("user give {string} as Value")
    public void user_give_as_Value(String string) {
        settings_TaxesPage.setTaxValueInput(string);
    }

    @Then("user click Add tax button")
    public void user_click_Add_tax_button() {
        settings_TaxesPage.clickAddTaxButton();
    }

    @Then("user verify that tax created")
    public void user_verify_that_tax_created() {
        settings_TaxesPage.highlightVerifyCreatedLabel();
    }

    @Then("user give {string} to change the Label")
    public void user_give_to_change_the_Label(String string) {
        settings_TaxesPage.setEditTaxLabel(string);
    }

    @Then("user give {string} to change the Value")
    public void user_give_to_change_the_Value(String string) {
        settings_TaxesPage.setEditTaxValue(string);
    }

    @Then("user verify that tax updated")
    public void user_verify_that_tax_updated() {
        settings_TaxesPage.highlightVerifyEditedLabelAndValue();
    }

    @Then("user click X to remove tax")
    public void user_click_X_to_remove_tax() {
        settings_TaxesPage.clickOnXToDelete();
    }


    @Then("user click on Taxes submodule")
    public void userClickOnTaxesSubmodule() {
        settings_TaxesPage.clickOnTaxesSubmoduleOfSettings();
    }
}
