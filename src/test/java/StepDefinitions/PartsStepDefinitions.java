package StepDefinitions;

import Utulities.BrowserUtils;
import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.ser.std.ObjectArraySerializer;
import io.cucumber.java.en.*;
import org.junit.Assert;
import pages.PartsPage;

public class PartsStepDefinitions {
    PartsPage partsPage =new PartsPage();

    @Then("user click file upload button")
    public void user_click_file_upload_button() {
       partsPage.clickCsvFileUploadButton();
        System.out.println("File uploaded");
    }

    @Then("user click choose file button")
    public void user_click_choose_file_button() {
       partsPage.clickChooseFileButton();
    }

    @Then("user click add part button")
    public void user_click_add_part_button() {
       partsPage.clickAddPartButton();
    }

    @Then("user give info about {string},{string},{string}")
    public void user_give_info_about(String string, String string2, String string3) {
        BrowserUtils.wait(1);
    partsPage.addPartWriteCode(string);
        BrowserUtils.wait(1);
    partsPage.addPartSelectType(string2);
        BrowserUtils.wait(1);
    partsPage.addPartWriteDescription(string3);
        BrowserUtils.wait(1);
    }

    @Then("user set dimensions {string},{string},{string}")
    public void user_set_dimensions(String string, String string2, String string3) {
       partsPage.setDimensions(string,string2,string3);
    }

    //  Then user select "<Charge>","<assembly>","<global>" info
    @Then("user select {string},{string},{string} info")
    public void user_select_info(String string, String string2, String string3) {
       partsPage.setAddPartClickChargeAssembly(string);
       partsPage.setAddPart_applyDiscountCheckBox(string2);
       partsPage.setAddPartClickGlobalCheckbox(string3);
    }

    //pieces per container must be from 1 to 255
    @Then("user set pieces per container {string}")
    public void user_set_pieces_per_container(String string) {
   partsPage.setAddPart_piecesPerContainer(string);
    }



    @Then("user verify that alert is displayed")
    public void user_verify_that_alert_is_displayed() {
       BrowserUtils.wait(1);
       partsPage.getErrorMessage();
    }

    @Then("user click scale prices button")
    public void user_click_scale_prices_button() {
       partsPage.clickScalePrice();
    }

    @Then("user set multiplier {string}")
    public void user_set_multiplier(String string) {
        partsPage.setScalePriceMultiplier(string);
    }

    @Then("user click apply button")
    public void user_click_apply_button() {
      partsPage.clickApplyButton();
    }

    @Then("user gets all parts list")
    public void user_gets_all_parts_list() {
         partsPage.getAllPartList();
    }



}
