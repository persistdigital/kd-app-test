Feature: Announcements

  As a user I should be able to send Announcements to the dealers

  Background:
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Announcements"

    Scenario: Verify that page header is Announcement
      Then user verify that page header is "Announcements"

     Scenario: User get list of Announcements number
       Given user get total number

       Scenario: User can add Announcement
         Then user click newAnnouncement button
         Then user give "Announcement1" as title
         Then user give "Happy New Year" as content
         Then user click create button

  Scenario: Verify that warning message is displayed when title and content box are empty
    Then user click newAnnouncement button
    Then user click create button
    And user verifies that "Title is required" message is displayed
    And user verifies that "Content is required" message is displayed

  Scenario: Delete Announcement
   #Then user writes "Announcement1" search button
    Then user click action button
    Then user click Delete Announcement Button
    Then user click delete button

  # this scenario includes all the steps above. It creates an announcement and deletes. Ready for regression suit.
  Scenario:Add and Delete Announcement
    Then add Announcement and delete Announcement
