
 Feature: RMA
 As user, I want to able to filter RMA

   @RMA

   Scenario: Verify header RMA
     Given user is on the login page
     And user logs in as "manufacturer"
     Then user navigates to "RMA"
     Then user verify that page header is "RMA"
