
Feature: Add dealers
  As a user , I should be able to add dealers after logging in

  @dealers
  Scenario: Add dealers
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Dealers"
    Then user Add dealer

  @negative_test_Dealers
  Scenario: Verify that warning message is displayed when select dealer is empty
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Dealers"
    Then user click add dealers button
    Then user click submit button
    Then user verify that warning messages are displayed
  |Dealer name is required|
  |Dealer address is required|
  |Phone is required         |
  |Dealer email is required  |

    # silinecek dealer ismini belirtirken dikkat et, bu ismi iceren tum dealerlar siliniyor(contain).
    # ismi tam yazarsan problem olmaz
  @deleteDealer
  Scenario: delete dealer
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Dealers"
    Then user delete dealers "exampleCompany"

    #smoke test icin kullanılacak. java faker kullanildi company uretmek icin
    Scenario: add and delete dealer
      Given user is on the login page
      And user logs in as "manufacturer"
      Then user navigates to "Dealers"
      Then user add and delete dealer for smoke test
