@orders
Feature: Orders

  Background: user navigates to orders page
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user is on the dashboard page
    And user navigate to "Orders" page

  @verify_Orders
  Scenario: User navigates to orders page
    Then verify that "Orders" page subtitle is displayed

  @verify_Edit_Order
  Scenario: User navigates to orders page
    Then user navigates to Edit Order page
    And verify that  Edit Order page subtitle is displayed

  @verify_Outstanding_and_Total_price_are_equal
  Scenario: Verify that Outstanding and Total price are equal
    Then user navigates to Edit Order page
    And verify that Outstanding and Total price are equal

  @verify_row_number_and_number_of_items_are_equal
  Scenario: Verify that Outstanding and Total price are equal
    Then user navigates to Edit Order page
    And verify that row number and number of items are equal

  @verify_order_status_same_in_order_status_and_in_table
  Scenario: Verify that Outstanding and Total price are equal
    Then user navigates to Edit Order page
    And verify order status same in order status and in table



