
Feature: Proposals
  As a user, I want to be able to give proposals

  @Proposals
  Scenario: create proposal
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Proposals"
    Then user verify that page header is "Proposals"

@createProposal
  Scenario: create proposal
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Proposals"
    Then user create proposal

  @proposalTable
  Scenario Outline: Login as <role> and create proposals
    Given user is on the login page
    And user logs in as "<role>"
    When user navigates to "<module>"
    Then user create proposal with this info "<companyName>","<collectionName>","<version>","<part>"

    Examples: manufacturer
      | role         |  module   | companyName| collectionName | version  |  part   |
      | manufacturer |  Proposal |  company1  | Spring         | version2 |  W0930  |
      | manufacturer |  Proposal |  company2  | Fall           | version1 |  WP1584 |
      | manufacturer |  Proposal |  Company 1 | Winter         | version3 |  W0930  |
      | manufacturer |  Proposal |  Company 1 | Summer         | version3 |  W0930  |
      | manufacturer |  Proposal |  Company 2 | Rose           | version2 |  WP1584 |
      | manufacturer |  Proposal |  Company 3 | Summer         | version1 |  WP1584 |

  @negative_test_Proposals_Step1
  Scenario: Verify that warning message is displayed when select dealer is empty
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Proposals"
    Then user click create proposal button
    Then user click next button
    And user verifies that "Please select a dealer" message is displayed
    And user verifies that "Please select location" message is displayed
    And user verifies that "Description is required" message is displayed
    And user verifies that "Sales representative is required" message is displayed

  @negative_test_Proposals_Step2
  Scenario: Verify that warning message is displayed when select dealer is empty
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Proposals"
    Then user click create proposal button
    Then user fill the form for "Company 1" and click next button
    Then user click next button
    And user verifies that "Collection is required" message is displayed
    And user verifies that "Version name is required" message is displayed

Scenario: delete proposal
  Given user is on the login page
  And user logs in as "manufacturer"
  Then user navigates to "Proposals"
  Then user delete proposals "Example Dealer"

  #chrome da geciyor ama firefox_headless da gecmiyor bu senaryo
  Scenario: manufacturer user create and delete proposal
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Proposals"
    Then user create proposal with this info "Example Dealer","Winter (DG)","V1","VAB48"
    Then user delete proposals "Example Dealer"