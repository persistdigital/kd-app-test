# new feature
# Tags: optional
@feedback
Feature: Feedback
  As a manufacturer user, I should be able to send message to dealers from feedback page

  Scenario Outline: user's message <message> is displayed
    Given user is on the login page
    When user logs in as "manufacturer"
    Then user navigate to "Feedback" page
    And user selects "<dealer>"
      # to send "<message>"
    Then verify that message "<message>" is displayed
    Examples:
      | message | dealer     |
      | Hello   | company1   |
      | Hello   | Company 2  |
      | Hello   | Company 3  |
      | Hello   | oz llc     |
      | Hello   | NewDealer1 |
      | Hello   | NewDealer2 |

