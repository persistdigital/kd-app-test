Feature: Inventory
  Background:
    Given user logs in as "manufacturer" and on the home page


  Scenario: user navigate Barcodes submodule
  Then user navigate to "Inventory" page "Barcodes" submodule
  Then user verify that page header is "Barcodes"


  Scenario: user navigate Purchase orders submodule
  Then user navigate to "Inventory" page "Purchase orders" submodule
  Then user verify that page header is "Purchase orders"
  Then user get total number

  @inventory
  Scenario: user navigate to Inventory page Activity submodule
  Then user navigate to "Inventory" page "Activity" submodule
  Then user verify that page header is "Activity"
  Then user verify that total item at the "Activity" module is"41"

  @inventory
  Scenario: user navigate to Inventory page Items submodule
    Then user navigate to "Inventory" page "Items" submodule
    Then user verify that page header is "Inventory"
    Then user verify that total item at the "Items" module is"40"