Feature: Settings_Taxes
  Background:
    Given user logs in as "manufacturer" and on the home page


    @SettingsTaxes
  Scenario: user can add tax and edit then delete
    Then user navigates to "Settings"
    Then user click on Taxes submodule
    Then user verify that page header is "Taxes"
    Then user give "TestTaxLabel" as Label
    Then user give "2" as Value
    Then user click Add tax button
    Then user verify that tax created
    Then user give "LabelChanged" to change the Label
    Then user give "5" to change the Value
    Then user verify that tax updated
    Then user click X to remove tax











