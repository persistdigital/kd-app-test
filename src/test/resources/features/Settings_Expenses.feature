Feature: Settings_Taxes
  Background:
    Given user logs in as "manufacturer" and on the home page


@SettingsExpensesAddCategory
  Scenario: user can add category and delete created category
    Then user navigates to "Settings"
    Then user click on Expense submodule
    Then user verify that page header is "Expenses"
    Then user highlight page header
    Then user give "TestCategory" as category name
    Then user click add button
    Then user verify that category created
    Then user click X to remove created category

@SettingsExpensesNegativeTestCreateVendor
  Scenario: user can not create vendor without filling vendor name
    Then user navigates to "Settings"
    Then user click on Expense submodule
    Then user verify that page header is "Expenses"
    Then user highlight page header
    Then user click Vendors
    Then user get total number
    Then user click Add Vendor button
    Then user click create button
    Then user verify that alert "Name is required" is displayed for Create Vendor

@SettingsExpensesCreateVendor
  Scenario: user can create, edit, and delete vendor
    Then user navigates to "Settings"
    Then user click on Expense submodule
    Then user verify that page header is "Expenses"
    Then user highlight page header
    Then user click Vendors
    Then user click Add Vendor button
    Then user give "TestVendor" as Vendor name
    Then user click create button
    Then user click Vendors
    Then user verify that Vendor created
    Then user search "TestVendor" created Vendor
    Then user verify that searched Vendor displayed
    Then user click on the three dots next to displayed Vendor
    Then user click on edit option
    Then user give "TestVendorNameChanged" as Vendor name
    Then user click on save button
    Then user click Vendors
    Then user verify that Vendor updated
    Then user search "TestVendorNameChanged" created Vendor
    Then user click on the three dots next to displayed Vendor
    Then user click on delete vendor option
    Then user click on expense delete button

  @SettingsExpensesAll
  Scenario: user can add category, delete created category, add tax, edit, and delete
    Then user navigates to "Settings"
    Then user click on Expense submodule
    Then user verify that page header is "Expenses"
    Then user highlight page header
    Then user give "TestCategory" as category name
    Then user click add button
    Then user verify that category created
    Then user click X to remove created category
    Then user click Vendors
    Then user get total number
    Then user click Add Vendor button
    Then user click create button
    Then user verify that alert "Name is required" is displayed for Create Vendor
    Then user give "TestVendor" as Vendor name
    Then user click create button
    Then user click Vendors
    Then user verify that Vendor created
    Then user search "TestVendor" created Vendor
    Then user verify that searched Vendor displayed
    Then user click on the three dots next to displayed Vendor
    Then user click on edit option
    Then user give "TestVendorNameChanged" as Vendor name
    Then user click on save button
    Then user click Vendors
    Then user verify that Vendor updated
    Then user search "TestVendorNameChanged" created Vendor
    Then user click on the three dots next to displayed Vendor
    Then user click on delete vendor option
    Then user click on expense delete button



