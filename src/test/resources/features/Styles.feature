Feature: create styles

  As a user, I want to be able to create styles

  @Styles
  Scenario: go to styles module
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Styles"


  @CreateStyles
  Scenario: create new styles module
    Given user logs in as "manufacturer" and on the home page
    Then user navigates to "Styles"
    Then user click add Style button
    Then user write "Flowers" as Style name
    Then user click create button

  @createCollection
  Scenario: create collection
    Given user logs in as "manufacturer" and on the home page
    Then user navigates to "Styles"
    Then user click add Collection button at the "Comfort" style
    Then user write "Blue" as collection name
    Then user write "MB" as collection short name
    Then user write "MB" as price group
    Then user click create button

  @Style_negative
  Scenario: go to styles module
    Given user logs in as "manufacturer" and on the home page
    Then user navigates to "Styles"
    Then user click add Collection button at the "Luxury" style
    Then user click create button
    Then verify that error messages are displayed

