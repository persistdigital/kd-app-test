@dashboard #@smoke_test
Feature: Dashboard
  As a user , I should be able to navigate dashboard after logging in

  Scenario: User navigates to dashboard page
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user is on the dashboard page
    And verify that "Active orders" "Dealers" "Active RMAs" buttons on the page

  Scenario: Retailer User navigates to dashboard page
    Given user is on the login page
    And user logs in as "retailer"
    Then user is on the dashboard page
    And verify that "Active proposals" "Active orders" on the page
      | Active proposals |
      | Active orders    |
    And verify that order status on the page
      | Draft                         |
      | Measurement Scheduled         |
      | Measurement done              |
      | Design done                   |
      | Proposal accepted             |
      | Cabinets ordered              |
      | Sent                          |
      | Vendor provided delivery date |
      | Vendor response approved      |
      | Vendor rejected               |
      | Completed                     |
      | Return initiated              |


