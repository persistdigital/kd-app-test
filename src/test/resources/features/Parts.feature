Feature: add new parts

  As a user, I want to be able to add new parts

Background:
  Given user is on the login page
  And user logs in as "manufacturer"
  Then user navigates to "Parts"

      @PartsGetHeader
      Scenario: go to parts module and get header
      Then user verify that page header is "Parts"

      @AddPartWithFileUpload
      Scenario: add part by uploading cvs file
      Then user click file upload button
      Then user set file path "src/test/resources/Parts-example.csv" for upload
      Then user click create button

      @CreateNewPart
      Scenario: click addPart button
      Then user click add part button
      Then user give info about "W0911","Wall","Wall cabinets"
      Then user click create button

      @AddPartNegativeScenario
      Scenario: try to add part without any info
      Then user click add part button
      Then user click create button
      And user verify that alert is displayed

    @AddPartWithDimensions
    Scenario: add part by scenario outline
    Then user click add part button
    Then user give info about "W960","Base","Base X"
    Then user set dimensions "2","3","4"
    Then user select "yes","no","yes" info
    Then user set pieces per container "50"
    Then user click create button

    @CreatePartsWithScenarioOutline
    Scenario Outline: add part by scenario outline
    Then user click add part button
    Then user give info about "<Code>","<Type>","<Description>"
    Then user set dimensions "<width>","<height>","<length>"
    Then user select "<charge assembly>","<apply discount>","<global>" info
    Then user set pieces per container "<piecesPerContainer>"
    Then user click create button

    Examples:
    |Code  |Description           |Type       |width|height |length   | charge assembly |apply discount|global  |piecesPerContainer|
    |x1    |Wall cabinets         |Wall       |12   |23     |15       |no               |Yes           |No      |4                 |
    |x2    |Valance               |Accessories|10   |13     |34       |no               |No            |yes     |6                 |
    |x3    |Base End Corner       |Base       |10   |23     |34       |yes              |no            |Yes     |9                 |
    |x4    |Wall Pantry           |Tall       |21   |21     |34       |Yes              |no            |yes     |7               |
    |x5    |Wall cabinets         |Wall       |19   |13     |21       |No               |Yes           |No      |3                 |

  @scalePrice
  Scenario: user scale prices
  Then user click scale prices button
  Then user set multiplier "0.5"
  And user click apply button

    @getALlPartsList
    Scenario: user gets parts list
      Then user gets all parts list


