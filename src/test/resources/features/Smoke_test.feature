@smoke_test
Feature: SmokeTest

  Scenario Outline: <role> user <header> page is displayed
    Given user is on the login page
    And user logs in as "<role>"
    Then user is on the dashboard page
    And user navigate to "<module>" page
    Then verify that "<header>" page title is displayed
    Examples: titles
      | role         | module        | header        |
      | manufacturer | Dealers       | Dealers       |
      | manufacturer | Proposals     | Proposals     |
      | manufacturer | Orders        | Orders        |
      | manufacturer | RMA           | RMA           |
      | manufacturer | Parts         | Parts         |
      | manufacturer | Resources     | Resources     |
      | manufacturer | Announcements | Announcements |
      | manufacturer | Expenses      | Expenses      |
      | manufacturer | Incomes       | Incomes       |
      | retailer     | Customers     | Customers     |
      | retailer     | Orders        | Orders        |
      | retailer     | RMA           | RMA           |
      | retailer     | Resources     | Resources     |

  Scenario: manufacturer user add and delete dealer
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Dealers"
    Then user add and delete dealer for smoke test

  Scenario: retailer user add and delete customer
    Given user is on the login page
    And user logs in as "retailer"
    Then user navigates to "Customer"
    Then user add new customer as "Smoke test customer" name
    Then user delete customer "Smoke test"