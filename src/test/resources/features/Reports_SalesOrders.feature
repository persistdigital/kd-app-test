

Feature: Reports_SalesOrders
  Background:
    Given user logs in as "manufacturer" and on the home page


  @verifySumOfTotalBalance
  Scenario: user can filter one mount interval then verify the total amount
  #  Scenario: user navigate Barcodes submodule
    Then user navigate to "Reports" page "Sales orders" submodule
    Then user verify that page header is "Sales orders"
    Then user give "6/24/2020" as start date
    Then user give "6/29/2020" as end date
#    Then user give "" as dealer name
    Then user click filter button
    Then user verify that filtered sales orders total amounts are correct


  @filterSalesOrders
   Scenario: user can put Start date, End date and dealer then filter
#  Scenario: user navigate Barcodes submodule
    Then user navigate to "Reports" page "Sales orders" submodule
    Then user verify that page header is "Sales orders"
     Then user give "6/23/2020" as start date
     Then user give "6/25/2020" as end date
     Then user give "company1" as dealer name
     Then user click filter button
     Then user verify that filtered dealer sales order "company1" displayed
     Then user click send email button
     Then user click send button


@Report_All_SalesOrders
Scenario: user can filter one mount interval then verify the total amount
  #  Scenario: user navigate Barcodes submodule
  Then user navigate to "Reports" page "Sales orders" submodule
  Then user verify that page header is "Sales orders"
  Then user give "6/24/2020" as start date
  Then user give "6/29/2020" as end date
#    Then user give "" as dealer name
  Then user click filter button
  Then user verify that filtered sales orders total amounts are correct
#  Scenario: user can put Start date, End date and dealer then filter
#  Scenario: user navigate Barcodes submodule
    Then user give "6/23/2020" as start date
    Then user give "6/25/2020" as end date
    Then user give "company1" as dealer name
    Then user click filter button
    Then user verify that filtered dealer sales order "company1" displayed
    Then user click send email button
    Then user click send button





