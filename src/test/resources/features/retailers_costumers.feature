Feature: Add costumers
  As a user , I should be able to add costumer after logging in

Background:
    Given user is on the login page
    And user logs in as "retailer"
    Then user navigates to "Customers"

  @addNewCustomer
  Scenario: add new customer
  Then user add new customer as "new customer" name

  @delete_customer
  Scenario: delete customer
  Then user delete customer "new customer"