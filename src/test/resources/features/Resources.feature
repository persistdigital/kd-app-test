Feature: Resources
  As a user I should able to navigate to see Resources

  Background:
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Resources"
    
  Scenario: Verify that page header is Resources
      Then user verify that page header is "Resources"

  Scenario: user get List of Resources number
      Then user get total number

   Scenario: user can add Resources
      Then user click addResource button
      Then user give "Deneme" as label
      Then user give "Other" as tag
      Then user set file path "src/test/resources/kitchen01.webp" for upload
      Then user click create button