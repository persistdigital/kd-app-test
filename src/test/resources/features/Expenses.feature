@Expenses
Feature: Expenses
  As a user I should able to navigate to see Expenses



Background:
    Given user is on the login page
    And user logs in as "manufacturer"
    Then user navigates to "Expenses"



  @notAddExpenses

  Scenario: user cannot add Expense without filling in required fields
#  Scenario: Verify that page header is Expenses
    Then user verify that page header is "Expenses"
#  Scenario: user get List of Expenses number
    Then user get total number
    Then user click addExpense button
    Then user click "Create" button
    Then user verify that alert "Vendor is required" is displayed
    Then user verify that alert "Amount is required" is displayed
    Then user verify that alert "Category is required" is displayed
    Then user click x to close Create Expense window


@addExpenses
   Scenario: user can add Expenses
    Then user click addExpense button
    Then user give "Examples" as vendor
    Then user give "New examples" as description
    Then user give "100" as amount
    Then user give "expenses 1" as category
#    Then user give "6/18/2020" as date
    Then user click "Create" button

@filterExpenses
  Scenario: user can filter expenses
    Then user filter "Examples" as vendor
    Then user filter "expenses 1" as category
#    Then user filter "6/18/2020" as start date
#    Then user filter "6/18/2020" as end date
    Then user click filter button
    Then user verify that filtered expense's "Examples" displayed
    Then user click reset button to cancel filter


@editExpenses
   Scenario: user can edit Expenses
    Then user click on the three dots next to the newly created expense
    Then user click on edit option
    Then user give "EditExamples" as vendor
    Then user give "New EditExamples" as description
    Then user give "200" as amount
    #  Then user give "a" as category
    Then user click "Save" button


@deleteExpenses
  Scenario: user can delete the previously created expense
    Then user click on the three dots next to the newly created expense
    Then user click on delete option
    Then user click on expense delete button


@allExpenses
  Scenario: user cannot add, add, filter, edit and delete Expense
#  Scenario: Verify that page header is Expenses
  Then user verify that page header is "Expenses"
#  Scenario: user get List of Expenses number
  Then user get total number
#  Scenario: user cannot add Expense without filling in required fields
  Then user click addExpense button
  Then user click "Create" button
  Then user verify that alert "Vendor is required" is displayed
  Then user verify that alert "Amount is required" is displayed
  Then user verify that alert "Category is required" is displayed
  Then user click x to close Create Expense window
#  Scenario: user can add Expenses
    Then user click addExpense button
    Then user give "Examples" as vendor
    Then user give "New examples" as description
    Then user give "100" as amount
    Then user give "expenses 1" as category
    Then user click "Create" button
#  Scenario: user can filter expenses
    Then user filter "Examples" as vendor
    Then user filter "expenses 1" as category
#    Then user filter "6/18/2020" as start date
#    Then user filter "6/18/2020" as end date
    Then user click filter button
    Then user verify that filtered expense's "Examples" displayed
    Then user click reset button to cancel filter
#  Scenario: user can edit Expenses
    Then user click on the three dots next to the newly created expense
    Then user click on edit option
    Then user give "EditExamples" as vendor
    Then user give "New EditExamples" as description
    Then user give "200" as amount
    #  Then user give "a" as category
    Then user click "Save" button
#  Scenario: user can delete the previously created expense
    Then user click on the three dots next to the newly created expense
    Then user click on delete option
    Then user click on expense delete button